# Seafile Photo Import

> Note, this project is in early development. Until version 1.0.0 is released, minor versions _may_ contain breaking changes. Breaking changes will be documented in the changelog.

## Overview

***What does this project do?***

Turns this...

```
/2020 Fireworks/fireworks_01.JPG
/2020 Fireworks/fireworks_02.JPG
/2020 Fireworks/fireworks_03.JPG
/August Camping trip 2020/IMG0001.JPG
/August Camping trip 2020/IMG0002.JPG
/August Camping trip 2020/IMG0003.JPG
/CameraBackup/JuneEvent2020.JPG
/CameraBackup/JuneEvent2020 (1).JPG
/CameraBackup/JuneEvent2020 (2).JPG
```

into this...

```
/2020/06/20200615T143045649.JPG
/2020/06/20200615T152304210.JPG
/2020/06/20200615T180621722.JPG
/2020/07/20200704T095256709.JPG
/2020/07/20200704T132719107.JPG
/2020/07/20200704T180621434.JPG
/2020/08/20200821T125452133.JPG
/2020/08/20200821T130827501.JPG
/2020/08/20200821T132719222.JPG
```

***Why would I want to do that?***

After spending years trying to organize photos by date, subject, event, or device, I realized that every naming structure has drawbacks. Attempts to organize always ended with a dozen different file naming patterns and inconsistent directory structure.

Today, there is little need to worry about organizing files since there is an endless supply of free software that will read EXIF data to categorize photos. As such, I just want some consistency to directory and file naming so that photos are no longer scattered across directories with vastly different filenames.

***Okay, I'm in. How can I get started?***

The quickest way to use this project is via the Docker image at [justinvoelker/seafile-photo-import](https://hub.docker.com/repository/docker/justinvoelker/seafile-photo-import). All of the configuration is through the following environment variables. Environment variables specifying a **default** are optional.

- **LOG_LEVEL** Default log level which can be error, warn, info, verbose, or debug _[default, warn]_
- **INTERVAL_SECONDS** Number of seconds between executions _[default, 3600]_
- **SEAFILE_USERNAME** Seafile username (usually an email address)
- **SEAFILE_PASSWORD** Seafile password
- **SEAFILE_HOSTNAME** Seafile hostname (including http(s)://)
- **TASK0_NAME** Name of this task (used in logging)
- **TASK0_ENABLED** Enable or disable the task _[default, true]_
- **TASK0_DRY_RUN** Actually rename and move files or just log the expected behavior _[default, false]_
- **TASK0_LOG_LEVEL** Task-specific log level to override default _[default, value of LOG\_LEVEL]_
- **TASK0_METADATA_MODIFIERS** Modify how the extended metadata is generated _[optional]_
- **TASK0_SRC_DIRECTORY** Source library and directory (includes all files in this and child directories)
- **TASK0_SRC_FILENAME_PATTERN** Regex pattern of files to be included _[default, .*]_
- **TASK0_SRC_FILENAME_DATETIME_PATTERN** Regex value to extract segments of original filename representing a date _(optional)_
- **TASK0_SRC_FILENAME_DATETIME_REPLACE** Regex datetime replacement value to be parsed for extended metadata _(optional, required if datetime pattern is provided)_
- **TASK0_SRC_FILENAME_DATETIME_TIMEZONE_PARSE** The timezone of the date parsed from the filename _(optional)_
- **TASK0_SRC_FILENAME_DATETIME_TIMEZONE_LOCAL** The desired local timezone for extended metadata _(optional, required if datetime timezone parse is provided)_
- **TASK0_DST_DIRECTORY** Destination library and directory
- **TASK0_DST_FILENAME_EXTENSION_CASE** Desired casing of file extension of new filename which can be _undefined_ (does not change casing), upper, or lower _[default, undefined]_
- **TASK0_DST_FILENAME_EXTENSION_CASE_PATTERN** Regex pattern defining group that represents the file extension _[default, .\*\\.(.*)]_
- **TASK0_DST_FILENAME_STEP0_NAME** Name of this step (used in logging) _(optional)_
- **TASK0_DST_FILENAME_STEP0_FIND** Regex value to find in filename _(optional, required if step name is provided)_
- **TASK0_DST_FILENAME_STEP0_REPL** Regex filename replacement value _(optional, required if step name is provided)_
- **TASK0_DST_FILENAME_STEP0_FLAG** Regex flags _(optional)_
- **TASK0_SIDECAR0_NAME** Name of this sidecar step (used in logging)
- **TASK0_SIDECAR0_FIND_PRIMARY_PATTERN** Regex value to extract segment of original primary filename _[default, (.\*)\\..*]_
- **TASK0_SIDECAR0_FIND_SIDECAR_REPLACE** Regex filename replacement value to find original sidecar filename
- **TASK0_SIDECAR0_NAME_PRIMARY_PATTERN** Regex value to extract segment of renamed primary filename _[default, (.\*)\\..*]_
- **TASK0_SIDECAR0_NAME_SIDECAR_REPLACE** Regex filename replacement value to rename sidecar filename
- **TASK0_SIDECAR0_REQUIRED** Skip renaming the primary file if the sidecar does not exist _[default, false]_

> Note, the zeros in _TASK0_, _STEP0_, and _SIDECAR0_ above should be replaced with incrementing integers to create multiple tasks, steps, and sidecar renames to be executed in numeric order. See examples below for suggestions.

***Can I really create such a simple directory structure?***

Maybe. The above example was my ideal end result. If you are only organzing photos from a digital camera, you can probably produce something equally simple.

Mobile devices, on the other hand, frequently take photos and videos at the same time. Motion Photos on Android and Live Photos on iOS are either photos with embedded videos (Android) or photos with separate-but-associated video files (iOS). Unfortunately, renaming these photos an videos can be problematic.

Photos from recent Android versions seemingly use ".MP.jpg" as a file extension to distinguish regular photos from photos with Motion Photo capabilities. These photos can be renamed to anything else and as long as the ".MP.jpg" extension is left as-is, Android recognizes the file as a Motion Photo. However, older Android versions prefixed file names with "MVIMG" to identify Motion Photos. Without that prefix, Android seemingly does not recognize the Motion Photo. As such, some files can be renamed to any desired value while others must follow a specific pattern.

Photos from iOS seemingly use a basic JPG file and a separate MOV file to create Live Photos. The base file name appears to be all that links the two files together. Because JPG and MOV files do not have the same EXIF metadata available, keeping both file names in sync while renaming can be difficult. To make renaming easier, the sidecar renaming functionality of this project can use metadata from one file to rename another. However, this carries significant risk since the MOV file representing the video portion of a Live Photo is indistinguishable from a stand-alone video file. Renaming a Live Photo MOV as if it were a stand-alone video would break the Live Photo functionalty for the associated photo.

Because of the two complications above, it may be wise to not rename photos and videos taken on mobile devices. The documentation for this project gives examples of both completely renaming files using metadata (produces a cleaner directory structure) and leaving existing files names as-is but organizing photos based on metadata (preserving mobile device photo functionality).

***Any gotchas I should be aware of?***

As mentioned above, certain circumstances may require that file names be left as-is. Otherwise, automatically renaming files based on metadata should be pretty straight forward but there are few scenarios where, if something went wrong, you may need to clean up a mess.

- **Live Photos on iOS are made up of a JPG and MOV file with the same base filename.** Automatically renaming them could break that connection if you're not careful. Using the sidecar renaming feature should prevent this from happening but mistakes are always possible. For example, renaming video files without first renaming photos will rename Live Photo MOV files as if they were stand alone videos rather than renaming them as a sidecar associated with a JPG.
- **Motion Photos on Android are a JPG file with an embedded video and are given the file extension *.MP.JPG*.** While renaming these Motion Photos should be straight forward, care must be taken to ensure the *.MP* portion of the file extension is preserved when it exists.
- **Motion Photos on older Android versions are a JPG file with an embedded video and are given the filename prefix *MVIMG** Removing the prefix from the file name will prevent Android from recognizing that the photo is a Motion Photo.
- **Photos with duplicate metadata could be automatically renamed in Seafile.** When moving files across directories, Seafile does not automatically overwrite files with the same name (which is good). Unfortunately, that means a desired filename of *20210722.JPG* could end up being *20210722 (1).JPG*. This unintentional renaming can be dangerous for iOS and Android photos.
  - Live Photos on iOS require precisely renaming two files to preserve their association. Automatic renaming could break that association, breaking the Live Photo functionality.
  - Motion Photos on Android require renaming a file and keeping the *.MP.JPG* extension. Seafile will only recognize *.JPG* as the file extension and name a file *20210722.MP (01).JPG*, breaking the Motion Photo functionality.
- **Some manufacturers do not include subsecond (millisecond) values in their EXIF data** This can produce the exact same datetime value for multiple photos taken in rapid succession (bursts). Using datetimes to name files would result in automatic renaming by Seafile which may have unintended consequences.

All of these potential problems can be mitigated with careful planning and proper task configuration. However, technology is not infallible. This project may contain bugs, EXIF data may be corrupt, Seafile may successfully rename an iOS Live Photo JPG but fail to rename the associated MOV, a network failure while moving a file may result in a second copy of the file, the list goes on. There is no perfect method of automatically renaming and moving thousands of files but hopefully errors are rare. The best method for protecting against worst case scenarios is to take regular backups and monitor the renaming process. A set-it-and-forget-it approach will eventually result in problems that, if caught early, could have been corrected.

## Metadata

The goal of this project is to rename and move files based on file metadata data such as device, dimensions, file type, or, as shown in the opening example above, date. The available metadata data can be viewed by creating a DRY_RUN task with DEBUG logging as shown in the first example task below. Once started, a series of messages will log to the console, one of which will contain the metadata. An extremely truncated example of that metadata is shown below.

```
{
  exiftool: {
    "FileTypeExtension": "jpg",
    "CreateDate": {
      "year": 2021,
      "month": 5,
      "day": 29,
      "hour": 8,
      "minute": 38,
      "second": 31,
      "millisecond": 000,
      "tzoffsetMinutes": -240,
      "rawValue": "2021:05:29 08:38:31",
      "zoneName": "UTC-4"
    },
    "ImageWidth": 4032,
    "ImageHeight": 3024,
  },
  extended: {
    CreateDate: {
      utc: {
        ISO8601: {
          extended: "2021-05-29T12:38:31.000",
          basic: "20210529T123831000",
        },
        year: { numeric: "2021", "2-digit": "21" },
        month: { numeric: "5", "2-digit": "05" },
        day: { numeric: "29", "2-digit": "29" },
        hour: { numeric: "12", "2-digit": "12" },
        minute: { numeric: "38", "2-digit": "38" },
        second: { numeric: "31", "2-digit": "31" },
        millisecond: { numeric: "0", "3-digit": "000" },
      },
    }
  }
}
```

### Examples

All of this metadata is available for use in the destination directory and filename replacement steps. For nested data, separate the object keys with a period. Here are some examples given the sample metadata above.

- `/Photos/{extended.CreateDate.utc.year.numeric}/{extended.CreateDate.utc.month.2-digit}/` produces `/Photos/2021/05/`
- `{extended.CreateDate.utc.ISO8601.basic}.{exiftool.FileTypeExtension}` produces `20210529T123831000.jpg`

> Note, it may be wise to use the existing file extension rather than **FileTypeExtension** for situations where the EXIF file type extension does not match the existing file extension. One such example is "mp4" files containing a **FileTypeExtension** of "mkv" (the reason for this difference is too lengthy to discuss here).

### ExifTool vs Extended

All of the metadata is broken down into two objects.

- **exiftool** contains the unaltered EXIF data exactly as it is returned from [exiftool-vendored](https://photostructure.github.io/exiftool-vendored.js/) (which in turn uses [ExifTool](https://exiftool.org/)).
- **extended** contains manipulated metadata based on the ExifTool EXIF data. For example, timezone-aware and zero-padded date values.

### Dates

Dates are likely the most complex piece of metadata. ExifTool does an incredible job of parsing the available date data into a consistent format. To further extend the usefulness of that data, the **extended** metadata object contains variations of all date objects by adding zero-padded and ISO8601 values for timezone-aware dates.

The following data is available for each date value.

- Timezone conversion
  - **utc** - the raw value is converted to the UTC datetime based on the timezone.
  - **local** - the raw value is converted to the local datetime based on the timezone.
- ISO8601 formats
  - **extended** - Date and time as ISO8601 with hyphens, colons, and periods for easier readability.
  - **basic** - Date and time as ISO8601 without hyphens, colons, or periods for wider file system naming compatibility.
- Truncated and padded values
  - Two-digit alternative date value for year.
  - Two-digit zero-padded date values for month, day, hour, minute, and second.
  - Three-digit zero-padded millisecond.

> Note, add **DATE_RAW_VALUE_UTC_AS_LOCAL** as a metadata modifier to change the timezone conversion for local values to fix an incorrect raw value. In specific scenarios (e.g. video files from Android Pixel devices) dates are incorrectly stored as a UTC value but formatted as a local value. This metadata modifier will correct that error to produce the correct local datetime.

Unfortunately, different devices capture date data differently. As such, some trial-and-error will be required to determine how to extract the best date data for a given device.

Even more unfortunately, some vendors store different raw datetime values between photos and videos. In such cases, it will be necessary to create different tasks for photos and videos, each using the appropriate timezone-aware date value.

My process for determining which datetime values to use involves taking a picture and a short video of the clock on my computer and viewing the metadata for those files. I then cross reference the time captured in the photo or video against the metadata datetimes to determine which values represent the datetime I'm looking for (local or UTC, etc).

### Timezones

Since timezones complicate date handling, here are a few general guidelines about when to use each value.

| If you want | and the date | use |
| ----------- | ------------ | --- |
| local datetime | contains timezone | local |
| local datetime | does not contain timezone data | _NA_ |
| UTC datetime | contains timezone | utc |
| UTC datetime | does not contain a timezone | _NA_ |

Notice that two options are _NA_. In these scenarios, the date does not contain timezone data which means there can be no conversion between timezones. As such, there are no options for _utc_ or _local_ and all of the extended metadata for that date uses the value as-is. For this reason, I prefer to name all of my files with local datetime as a sort of "least common denominator" since not all of my devices store timezone data and those that do can be converted to a local time.

Typically, DSLR and point-and-shoot cameras will not contain timezone data and will not have timezone-aware values. As such, the only available date objects will represent local time only (the time set on the camera). Conversely, mobile devices typically _do_ contain timezone data and can use any timezone-aware value. Although, for the sake of consistency, using **local** for photos from mobile devices will produce local datetime values that align with the values from devices that do not contain timezone data and are assumed to already be local values.

> Note, as mentioned in the Dates section above, when using a _local_ datetime value, it may be necessary to add a metadata modifier to fix incorrect EXIF values.

## Logging

There are two aspects to logging: console logging and edit logging.

### Console logging

Once running, the console will log various information based on log level set in the config. Set to **warn** by default, log level can be configured for the entire project and/or for a given task. Each log level logs progressively more information.

- **error**: something is stopping the process from continuing
- **warn**: something unusual happened that you should be aware of
- **info**: logging of actions that manipulate data (rename, move)
- **verbose**: general information about the background processes taking place
- **debug**: as much information as possible about everything that is happening

Typically, **debug** is used in combination with a dry run to learn more about what EXIF data is available, how it is being inserted into directory names, and how the various find and replace steps are manipulating the resulting file name.

### Edit logging

For safe measure, all file renames and moves are logged to a dedicated file at **/var/log/edits-YYYY\[.dryrun\].log**. Although this same data is available within the **info** log level, this dedicated file will contain only the actions that change file names and locations and nothing else. Consider it an audit trail of actions taken against the source data.

## Example Configurations

The general idea is to setup tasks according to differences in source data. For example, different manufactures use different file names and EXIF data. Even photos and videos from the same manufactures may use different EXIF data.

Though a single task will typically process a single directory, there are situations that require multiple tasks for a single directory (see iOS config example). To demonstrate this, and other examples, below are the tasks I use for organizing my own photos.

Personally, I like to be restrictive to prevent accidental processing of unknown files. Even though all files in the "Canon DSLR" directory should have the same source file name format, if an errant file finds its way into that directory, I do not want to process it. That file should be left as-is to avoid processing a photo that, for example, may not have expected EXIF data.

> Note, environment variables set in a docker-compose.yml file must escape dollar signs with an additional dollar sign. For example, to use `^.*\.jpg$` to find all *.jpg files, the environment variable value must be `^.*\.jpg$$`. Another example is using `$1` as a regex group value which must be `$$1`.

### Reading EXIF data

This task is only for displaying EXIF data since it can be difficult to identify the correct value for file and directory names. Setting **DRY_RUN** to true will prevent files from being renamed or moved and setting **LOG_LEVEL** to debug will log the full EXIF data extracted from the photo.

> Note, destination directory is required but since this is a dry run, no photos will be moved and the destination directory is irrelevant.

```
- TASK0_NAME=Debug for EXIF data
- TASK0_DRY_RUN=TRUE
- TASK0_LOG_LEVEL=debug
- TASK0_SRC_DIRECTORY=/Photo Imports Testing/Debug EXIF Data/
- TASK0_DST_DIRECTORY=/Photo Imports Testing/Debug EXIF Data/
```

### Canon DSLR

Observations:

- File names are sequential integers (e.g. IMG_2721.JPG or MVI_2722.MOV)
- Camera settings allow for setting a date and time, but not a timezone
- SubSecDateTimeOriginal millseconds are always zero except for photos taken in burst mode

Task setup:

- Restrict the task to processing files in the "Canon EOS 600D" directory of the "Photo Imports" library
- Only process files matching the expected filename pattern of "IMG_0000.JPG" or "MVI_0000.MOV"
- Move files to the "Photos" library into a directory hierarchy composed of the EXIF year and month followed by hard-coded device
- Replace the filename with extended.SubSecDateTimeOriginal.ISO8601.basic, leaving the file extension as-is

Environment variables:

```
- TASK1_NAME=Canon EOS 600D
- TASK1_SRC_DIRECTORY=/Photo Imports/Canon EOS 600D/
- TASK1_SRC_FILENAME_PATTERN=^(IMG|MVI)_\d{4}\.(JPG|MOV)$$
- TASK1_DST_DIRECTORY=/Photos/{extended.SubSecDateTimeOriginal.year.numeric}/{extended.SubSecDateTimeOriginal.month.2-digit}/Canon EOS 600D/
- TASK1_DST_FILENAME_STEP0_NAME=Replace file name
- TASK1_DST_FILENAME_STEP0_FIND=^.*(JPG|MOV)$$
- TASK1_DST_FILENAME_STEP0_REPL={extended.SubSecDateTimeOriginal.ISO8601.basic}.$$1
```

### Nikon point-and-shoot

Observations:

- File names are sequential integers (e.g. DSCN0272.JPG or DSCN0273.MOV)
- Camera settings allow for setting a date, time, and timezone, but timezone does not appear in EXIF data
- No subsecond datetimes exist and milliseconds is always equals 0

Task setup:

- Restrict the task to processing files in the "Nikon COOLPIX S8100" directory of the "Photo Imports" library
- Only process files matching the expected filename pattern of "DSCN0000.JPG" or "DSCN0000.MOV"
- Move files to the "Photos" library into a directory hierarchy composed of the EXIF year and month followed by hard-coded device
- Replace the filename with extended.DateTimeOriginal.ISO8601.basic, leaving the file extension as-is

Environment variables:

```
- TASK2_NAME=Nikon COOLPIX S8100
- TASK2_SRC_DIRECTORY=/Photo Imports/Nikon COOLPIX S8100/
- TASK2_SRC_FILENAME_PATTERN=^DSCN\d{4}\.(JPG|MOV)$$
- TASK2_DST_DIRECTORY=/Photos/{extended.DateTimeOriginal.year.numeric}/{extended.DateTimeOriginal.month.2-digit}/Nikon COOLPIX S8100/
- TASK2_DST_FILENAME_STEP0_NAME=Replace file name
- TASK2_DST_FILENAME_STEP0_FIND=^.*(JPG|MOV)$$
- TASK2_DST_FILENAME_STEP0_REPL={extended.DateTimeOriginal.ISO8601.basic}.$$1
```

### Android

Observations:

- File names are UTC date and time stamps (e.g. PXL_20210605_124827617.jpg or PXL_20210604_085227274.mp4)
- Photos containing an embedded Motion Photo include ".MP" before the ".jpg" extension (e.g. PXL_20210605_124827617.MP.jpg)
- Video CreateDate, TrackCreateDate, and MediaCreateDate datetimes appear to represent the datetime the video stopped recording
- Video CreateDate, TrackCreateDate, and MediaCreateDate datetimes appear to represent the UTC datetime incorrectly formatted as a local datetime with timezone data
- The only reliable datetime appears to be the filename which represents the UTC datetime the photo was taken or the video started recording

Task setup:

- Restrict the task to processing files in the "Google Pixel 2" directory of the "Photo Imports" library
- Only process files matching the expected filename pattern of "PXL_00000000_000000000" with "jpg" (with possible ".MP") or "mp4" extension
- Parse filename as a consistent source of datetime that exists for both photos and videos (to avoid needing two tasks, one for photos and one for videos)
  - Photos and videos both contain CreateDate but the photos store that value as local datetime whereas videos store that value as UTC (incorrectly formatted as local)
  - Photos contain DateTimeOriginal but videos contain TrackCreateDate and MediaCreateDate
- Move files to the "Photos" library into a directory hierarchy composed of the EXIF year and month followed by hard-coded device
- Leave file names as-is

Environment variables (no file renaming):

```
- TASK3_NAME=Google Pixel 2
- TASK3_SRC_DIRECTORY=/Photo Imports/Google Pixel 2/
- TASK3_SRC_FILENAME_PATTERN=^PXL_\d{8}_\d{9}\.((MP\.)?jpg|mp4)$$
- TASK3_SRC_FILENAME_DATETIME_PATTERN=^PXL_(\d{4})(\d{2})(\d{2})_(\d{2})(\d{2})(\d{2})(\d{3}).*$$
- TASK3_SRC_FILENAME_DATETIME_REPLACE=$$1-$$2-$$3T$$4:$$5:$$6.$$7
- TASK3_SRC_FILENAME_DATETIME_TIMEZONE_PARSE=UTC
- TASK3_SRC_FILENAME_DATETIME_TIMEZONE_LOCAL={exiftool.tz}
- TASK3_DST_DIRECTORY=/Photos/{extended.FilenameDate.local.year.numeric}/{extended.FilenameDate.local.month.2-digit}/Google Pixel 2/
```

Alternate environment variables (renaming files via filename datetime):

This example parses the file name as a UTC datetime and uses it to rename the files to a local datetime. Although this works for recent Android versions (starting mid September 2020 on my Pixel 2) older versions did not follow the same file naming pattern. For example, older file names did not contain milliseconds and Motion Photos were prefixed with "MVIMG" and are not recognized as motion photos without that prefix. For these reasons, the following example tasks for renaming Android photos is not recommended but is shown only as an example of a complex task.

```
- TASK3_NAME=Google Pixel 2
- TASK3_SRC_DIRECTORY=/Photo Imports/Google Pixel 2/
- TASK3_SRC_FILENAME_PATTERN=^PXL_\d{8}_\d{9}(\.MP)?\.jpg$$
- TASK3_SRC_FILENAME_DATETIME_PATTERN=^PXL_(\d{4})(\d{2})(\d{2})_(\d{2})(\d{2})(\d{2})(\d{3}).*$$
- TASK3_SRC_FILENAME_DATETIME_REPLACE=$$1-$$2-$$3T$$4:$$5:$$6.$$7
- TASK3_SRC_FILENAME_DATETIME_TIMEZONE_PARSE=UTC
- TASK3_SRC_FILENAME_DATETIME_TIMEZONE_LOCAL={exiftool.tz}
- TASK3_DST_DIRECTORY=/Photos/{extended.FilenameDate.local.year.numeric}/{extended.FilenameDate.local.month.2-digit}/Google Pixel 2/
- TASK3_DST_FILENAME_EXTENSION_CASE=upper
- TASK3_DST_FILENAME_STEP0_NAME=Replace file name
- TASK3_DST_FILENAME_STEP0_FIND=^.*?\.((MP\.)?jpg|mp4)$$
- TASK3_DST_FILENAME_STEP0_REPL={extended.FilenameDate.local.ISO8601.basic}.$$1

```

Alternate environment variables (renaming files via EXIF datetime):

This example uses SubSecDateTimeOriginal for photos and MediaCreateDate with metadata modifier for videos. Although this works for recent Android versions (starting mid September 2020 on my Pixel 2) older versions did not follow the same file naming pattern. For example, older file names did not contain milliseconds and Motion Photos were prefixed with "MVIMG" and are not recognized as motion photos without that prefix. For these reasons, the following example tasks for renaming Android photos is not recommended but is shown only as an example of a complex task.

Because the original filename is the only piece of data that contains the datetime that the video started recording (i.e. it is not available as a piece of metadata) the task example below is lossy and results in eliminating data that cannot be recovered except by digging through logs. For this reason, the alterative environment variables example above that uses the FilenameDate is recommended if file renaming is desired.

```
- TASK3_NAME=Google Pixel 2 (photo)
- TASK3_SRC_DIRECTORY=/Photo Imports/Google Pixel 2/
- TASK3_SRC_FILENAME_PATTERN=^PXL_\d{8}_\d{9}\.(MP\.)?jpg$$
- TASK3_DST_DIRECTORY=/Photos/{extended.SubSecDateTimeOriginal.local.year.numeric}/{extended.SubSecDateTimeOriginal.local.month.2-digit}/Google Pixel 2/
- TASK3_DST_FILENAME_EXTENSION_CASE=upper
- TASK3_DST_FILENAME_STEP0_NAME=Replace file name
- TASK3_DST_FILENAME_STEP0_FIND=^.*?\.((MP\.)?jpg)$$
- TASK3_DST_FILENAME_STEP0_REPL={extended.SubSecDateTimeOriginal.local.ISO8601.basic}.$$1
- TASK4_NAME=Google Pixel 2 (video)
- TASK4_METADATA_MODIFIERS=DATE_RAW_VALUE_UTC_AS_LOCAL
- TASK4_SRC_DIRECTORY=/Photo Imports/Google Pixel 2/
- TASK4_SRC_FILENAME_PATTERN=^PXL_\d{8}_\d{9}\.mp4$$
- TASK4_DST_DIRECTORY=/Photos/{extended.MediaCreateDate.local.year.numeric}/{extended.MediaCreateDate.local.month.2-digit}/Google Pixel 2/
- TASK4_DST_FILENAME_STEP0_NAME=Replace file name
- TASK4_DST_FILENAME_STEP0_FIND=^.*$$
- TASK4_DST_FILENAME_STEP0_REPL={extended.MediaCreateDate.local.ISO8601.basic}.MP4
```

### iOS

Observations:

- File names are sequential integers (e.g. IMG_0387.JPG or IMG_0388.MOV)
- Live photos are comprised of a photo and video file with the same name
- Video CreateDate, TrackCreateDate, and MediaCreateDate datetimes appear to represent a datetime sometime after the video stopped recording
- Video CreateDate, TrackCreateDate, and MediaCreateDate datetimes appear to represent the UTC datetime incorrectly formatted as a local datetime with no timezone data
- The only reliable datetimes with correctly formatted local datetime values appear to be
  - For photos, DateTimeOriginal (or SubSecDateTimeOriginal)
  - For videos, CreationDate

Task setup:

- Restrict the task to processing files in the "Apple iPhone 7" directory of the "Photo Imports" library
- Only process files matching the expected filename pattern of "IMG_0000" with "JPG" or "MOV" extension
- Move files to the "Photos" library into a directory hierarchy composed of the EXIF year and month followed by hard-coded device
- If present, include Live Photo video file when processing photos (these could be caught with the "video" task but slight differences in EXIF datetimes could result in a Live Photo video file being moved to a different folder than the related photo if the photo was taken near midnight and the datetimes are off by just a single second).

Environment variables (no file renaming):

```
- TASK5_NAME=Apple iPhone 7 (photo)
- TASK5_SRC_DIRECTORY=/Photo Imports/Apple iPhone 7/
- TASK5_SRC_FILENAME_PATTERN=^IMG_\d{4}.JPG$$
- TASK5_DST_DIRECTORY=/Photos/{extended.DateTimeOriginal.local.year.numeric}/{extended.DateTimeOriginal.local.month.2-digit}/Apple iPhone 7/
- TASK5_SIDECAR0_NAME=Live Photo video file
- TASK5_SIDECAR0_FIND_SIDECAR_REPLACE=$$1.MOV
- TASK5_SIDECAR0_NAME_SIDECAR_REPLACE=$$1.MOV
- TASK6_NAME=Apple iPhone 7 (video)
- TASK6_SRC_DIRECTORY=/Photo Imports/Apple iPhone 7/
- TASK6_SRC_FILENAME_PATTERN=^IMG_\d{4}.MOV$$
- TASK6_DST_DIRECTORY=/Photos/{extended.CreationDate.local.year.numeric}/{extended.CreationDate.local.month.2-digit}/Apple iPhone 7/
```

Alternate environment variables (renaming files via EXIF datetime):

This example uses SubSecDateTimeOriginal for photos and CreationDate for videos. Because Live Photos are comprised of two different files that share the same bas file name, the following example tasks for renaming iOS photos is not recommended but is shown only as an example of a complex task.

```
- TASK5_NAME=Apple iPhone 7 (photo)
- TASK5_SRC_DIRECTORY=/Photo Imports/Apple iPhone 7/
- TASK5_SRC_FILENAME_PATTERN=^IMG_\d{4}.JPG$$
- TASK5_DST_DIRECTORY=/Photos/{extended.SubSecDateTimeOriginal.local.year.numeric}/{extended.SubSecDateTimeOriginal.local.month.2-digit}/Apple iPhone 7/
- TASK5_DST_FILENAME_STEP0_NAME=Replace file name
- TASK5_DST_FILENAME_STEP0_FIND=^.*(JPG)$$
- TASK5_DST_FILENAME_STEP0_REPL={extended.SubSecDateTimeOriginal.local.ISO8601.basic}.$$1
- TASK5_SIDECAR0_NAME=Live Photo associated video file
- TASK5_SIDECAR0_FIND_SIDECAR_REPLACE=$$1.MOV
- TASK5_SIDECAR0_NAME_SIDECAR_REPLACE=$$1.MOV
- TASK6_NAME=Apple iPhone 7 (video)
- TASK6_SRC_DIRECTORY=/Photo Imports/Apple iPhone 7/
- TASK6_SRC_FILENAME_PATTERN=^IMG_\d{4}.MOV$$
- TASK6_DST_DIRECTORY=/Photos/{extended.CreationDate.local.year.numeric}/{extended.CreationDate.local.month.2-digit}/Apple iPhone 7/
- TASK6_DST_FILENAME_STEP0_NAME=Replace file name
- TASK6_DST_FILENAME_STEP0_FIND=^.*(MOV)$$
- TASK6_DST_FILENAME_STEP0_REPL={extended.CreationDate.local.ISO8601.basic}.$$1
```

## Development

A couple notes on the development of this project.

- The **main** branch always represents the latest code
- Commit messages are formatted according to [Conventional Commits](https://conventionalcommits.org/)
- Version numbers follow [Semantic Versioning](https://semver.org/) (the current, pre-1.0.0 releases may contain breaking changes in minor versions)
