# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.2.1](https://gitlab.com/justinvoelker/seafile-photo-import/compare/v0.2.0...v0.2.1) (2021-08-01)

## [0.2.0](https://gitlab.com/justinvoelker/seafile-photo-import/compare/v0.1.3...v0.2.0) (2021-08-01)


### ⚠ BREAKING CHANGES

* extended metadata "local2" has been removed and should
be replaced with "local" with DATE_RAW_VALUE_UTC_AS_LOCAL added as a
metadata modifier.
* refactor metadata into either "exiftool" or "extended"

### Features

* add config to change case of file extension ([0e7eaaa](https://gitlab.com/justinvoelker/seafile-photo-import/commit/0e7eaaa5f40d96eaf6938bfbee6e6f4a6f0bc783))
* add sidecar renaming ([d3636df](https://gitlab.com/justinvoelker/seafile-photo-import/commit/d3636dfd139439786ed14d1a62f96b7d7931cab2))
* extended metadata for date-based filenames ([fcb8558](https://gitlab.com/justinvoelker/seafile-photo-import/commit/fcb85586614650e0d6241dff12e56a0a8e8de2b7))
* remove local2 from extended metadata dates ([b6ff4af](https://gitlab.com/justinvoelker/seafile-photo-import/commit/b6ff4af1af97bfe3aaa04eb2605387c1bec04e76))
* separate custom metadata from exif data ([7f6f4da](https://gitlab.com/justinvoelker/seafile-photo-import/commit/7f6f4da4906ad6e155662a3c417f03cf29e09184))

### [0.1.3](https://gitlab.com/justinvoelker/seafile-photo-import/compare/v0.1.2...v0.1.3) (2021-06-13)


### Features

* add ISO8601LOCAL2 datetime value ([dd4a67a](https://gitlab.com/justinvoelker/seafile-photo-import/commit/dd4a67aed448cf05037e2acb99984e91e0d917d8))


### Bug Fixes

* moveFile should do nothing if directories match ([2bf5b4d](https://gitlab.com/justinvoelker/seafile-photo-import/commit/2bf5b4d6a306e104e295089ac2ac4c931149b9ad))

### [0.1.2](https://gitlab.com/justinvoelker/seafile-photo-import/compare/v0.1.1...v0.1.2) (2021-06-13)


### Bug Fixes

* do not log if rename/move failed ([209a35b](https://gitlab.com/justinvoelker/seafile-photo-import/commit/209a35b0d92cf7a6fb8a1cb106d99fec97f90026))
* parent_dir sometimes missing trailing slash ([9e7f4ba](https://gitlab.com/justinvoelker/seafile-photo-import/commit/9e7f4ba0b6942ffde6e7299588ba4c6da52a053e))

### [0.1.1](https://gitlab.com/justinvoelker/seafile-photo-import/compare/v0.1.0...v0.1.1) (2021-06-12)


### Bug Fixes

* destination directory not being created ([922c0fc](https://gitlab.com/justinvoelker/seafile-photo-import/commit/922c0fc5ebc79a2289cdb55aede067188eb59c51))
* incorect API call for file move ([0cbdb78](https://gitlab.com/justinvoelker/seafile-photo-import/commit/0cbdb782fe9aaa9e696084b7946b12e026176b85))

## 0.1.0 (2021-06-12)
