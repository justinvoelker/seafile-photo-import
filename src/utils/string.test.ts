import { stripExtraSlashes } from "./string";

describe("stripExtraSlashes", () => {
  it("should strip no slashes", () => {
    expect(stripExtraSlashes("Library/Directory")).toBe("Library/Directory");
  });

  it("should strip multiple slashes", () => {
    expect(stripExtraSlashes("///Library//Directory///")).toBe(
      "Library/Directory"
    );
  });
});
