import { getListHash } from "./crypto";

describe("getListHash", () => {
  it("should return hash of list", () => {
    const list = [{ name: "file1" }, { name: "file2" }];

    const hash = getListHash(list);

    expect(hash).toBe("60ab8257a595b92b0189be8d33e4c7af");
  });
});
