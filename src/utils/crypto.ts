import crypto from "crypto";

export function getListHash(list: Record<string, unknown>[]): string {
  return crypto.createHash("md5").update(JSON.stringify(list)).digest("hex");
}
