import { logger } from "./logger";
import { createDirectoryHierarchy } from "./directory";
import * as seafile from "./seafile";

jest.mock("./config", () => ({
  config: jest.fn(() => {
    return {
      logLevel: "warn",
      seafile: { hostname: "host", username: "user", password: "pass" },
    };
  }),
}));

describe("createDirectoryHierarchy", () => {
  it("should create no directory", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const mockDirectoryExists = jest
      .spyOn(seafile, "directoryExists")
      .mockResolvedValue(false);
    const mockCreateDirectory = jest
      .spyOn(seafile, "createDirectory")
      .mockImplementation();

    await createDirectoryHierarchy({
      libraryId: "00000000",
      libraryName: "MyLibrary",
      directory: "/",
      dryRun: false,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(mockDirectoryExists).toHaveBeenCalledTimes(0);
    expect(mockCreateDirectory).toHaveBeenCalledTimes(0);
  });

  it("should create single-level directory", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const mockDirectoryExists = jest
      .spyOn(seafile, "directoryExists")
      .mockResolvedValue(false);
    const mockCreateDirectory = jest
      .spyOn(seafile, "createDirectory")
      .mockImplementation();

    await createDirectoryHierarchy({
      libraryId: "00000000",
      libraryName: "MyLibrary",
      directory: "/2021",
      dryRun: false,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(mockDirectoryExists).toHaveBeenCalledTimes(1);
    expect(mockDirectoryExists).toHaveBeenNthCalledWith(1, {
      libraryId: "00000000",
      path: "/2021",
    });
    expect(mockCreateDirectory).toHaveBeenCalledTimes(1);
    expect(mockCreateDirectory).toHaveBeenNthCalledWith(1, {
      dryRun: false,
      libraryId: "00000000",
      libraryName: "MyLibrary",
      path: "/2021",
    });
  });

  it("should create multi-level directory", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const mockDirectoryExists = jest
      .spyOn(seafile, "directoryExists")
      .mockResolvedValue(false);
    const mockCreateDirectory = jest
      .spyOn(seafile, "createDirectory")
      .mockImplementation();

    await createDirectoryHierarchy({
      libraryId: "00000000",
      libraryName: "MyLibrary",
      directory: "/2021/05",
      dryRun: false,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(mockDirectoryExists).toHaveBeenCalledTimes(2);
    expect(mockDirectoryExists).toHaveBeenNthCalledWith(1, {
      libraryId: "00000000",
      path: "/2021",
    });
    expect(mockDirectoryExists).toHaveBeenNthCalledWith(2, {
      libraryId: "00000000",
      path: "/2021/05",
    });
    expect(mockCreateDirectory).toHaveBeenCalledTimes(2);
    expect(mockCreateDirectory).toHaveBeenNthCalledWith(1, {
      dryRun: false,
      libraryId: "00000000",
      libraryName: "MyLibrary",
      path: "/2021",
    });
    expect(mockCreateDirectory).toHaveBeenNthCalledWith(2, {
      dryRun: false,
      libraryId: "00000000",
      libraryName: "MyLibrary",
      path: "/2021/05",
    });
  });

  it("should partially create multi-level directory", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const mockDirectoryExists = jest
      .spyOn(seafile, "directoryExists")
      .mockResolvedValueOnce(true)
      .mockResolvedValueOnce(false);
    const mockCreateDirectory = jest
      .spyOn(seafile, "createDirectory")
      .mockImplementation();

    await createDirectoryHierarchy({
      libraryId: "00000000",
      libraryName: "MyLibrary",
      directory: "/2021/05",
      dryRun: false,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(mockDirectoryExists).toHaveBeenCalledTimes(2);
    expect(mockDirectoryExists).toHaveBeenNthCalledWith(1, {
      libraryId: "00000000",
      path: "/2021",
    });
    expect(mockDirectoryExists).toHaveBeenNthCalledWith(2, {
      libraryId: "00000000",
      path: "/2021/05",
    });
    expect(mockCreateDirectory).toHaveBeenCalledTimes(1);
    expect(mockCreateDirectory).toHaveBeenNthCalledWith(1, {
      dryRun: false,
      libraryId: "00000000",
      libraryName: "MyLibrary",
      path: "/2021/05",
    });
  });
});
