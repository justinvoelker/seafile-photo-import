import { logger } from "./logger";

import * as sidecar from "./sidecar";
import * as seafile from "./seafile";
import { ensureSidecarFileExists, getSidecarFilename } from "./sidecar";

jest.mock("./config", () => ({
  config: jest.fn(() => {
    return {
      logLevel: "warn",
      seafile: { hostname: "host", username: "user", password: "pass" },
    };
  }),
}));

describe("ensureSidecarFileExists", () => {
  it("should do nothing is file is not required", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const sidecarGetSidecarFilenameSpy = jest
      .spyOn(sidecar, "getSidecarFilename")
      .mockImplementation();
    const seafileFileExistsSpy = jest
      .spyOn(seafile, "fileExists")
      .mockImplementation();

    await ensureSidecarFileExists({
      libraryId: "00000000",
      primaryFileName: "IMG_0001.JPG",
      primaryFilePath: "/path",
      findPrimaryPattern: "(.*)\\..*",
      findSidecarReplace: "$1.MOV",
      required: false,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(sidecarGetSidecarFilenameSpy).toHaveBeenCalledTimes(0);
    expect(seafileFileExistsSpy).toHaveBeenCalledTimes(0);
  });

  it("should do execute successfully if file is required and exists", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const sidecarGetSidecarFilenameSpy = jest
      .spyOn(sidecar, "getSidecarFilename")
      .mockImplementation();
    const seafileFileExistsSpy = jest
      .spyOn(seafile, "fileExists")
      .mockResolvedValue(true);

    await ensureSidecarFileExists({
      libraryId: "00000000",
      primaryFileName: "IMG_0001.JPG",
      primaryFilePath: "/path",
      findPrimaryPattern: "(.*)\\..*",
      findSidecarReplace: "$1.MOV",
      required: true,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(sidecarGetSidecarFilenameSpy).toHaveBeenCalledTimes(1);
    expect(seafileFileExistsSpy).toHaveBeenCalledTimes(1);
  });

  it("should throw an error if file is required and does not exist", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const sidecarGetSidecarFilenameSpy = jest
      .spyOn(sidecar, "getSidecarFilename")
      .mockImplementation();
    const seafileFileExistsSpy = jest
      .spyOn(seafile, "fileExists")
      .mockResolvedValue(false);

    expect.assertions(4);
    try {
      await ensureSidecarFileExists({
        libraryId: "00000000",
        primaryFileName: "IMG_0001.JPG",
        primaryFilePath: "/path",
        findPrimaryPattern: "(.*)\\..*",
        findSidecarReplace: "$1.MOV",
        required: true,
      });
    } catch (err) {
      expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
      expect(sidecarGetSidecarFilenameSpy).toHaveBeenCalledTimes(1);
      expect(seafileFileExistsSpy).toHaveBeenCalledTimes(1);
      expect(err).toContain("Unable to find sidecar file");
    }
  });
});

describe("getSidecarFilename", () => {
  it("should return a filename with regex replacement", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const filename = getSidecarFilename({
      name: "IMG_0001.JPG",
      find: "(.*)\\..*",
      replace: "$1.MOV",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(filename).toBe("IMG_0001.MOV");
  });
});
