import { axios } from "./axios";
import * as seafile from "./seafile";

jest.mock("./config", () => ({
  config: jest.fn(() => {
    return {
      seafile: { hostname: "host", username: "user", password: "pass" },
    };
  }),
}));

describe("Axios instance", () => {
  it("should use values from config", () => {
    expect(axios.defaults.baseURL).toBe("host");
    expect(axios.defaults.timeout).toBe(1000);
  });
});

describe("Axios instance interceptor", () => {
  it("should add an authorization header but only call getAuthToken once", async () => {
    const seafileGetAuthTokenSpy = jest
      .spyOn(seafile, "getAuthToken")
      .mockResolvedValueOnce("1234")
      .mockResolvedValueOnce("5678");

    const result = await (axios.interceptors
      .request as any).handlers[0].fulfilled({ headers: {} });
    const result2 = await (axios.interceptors
      .request as any).handlers[0].fulfilled({ headers: {} });

    expect(seafileGetAuthTokenSpy).toHaveBeenCalledTimes(1);
    expect(result.headers.Authorization).toBe("Token 1234");
    expect(result2.headers.Authorization).toBe("Token 1234");
  });

  it("should throw error if rejected", async () => {
    expect.assertions(1);
    try {
      await (axios.interceptors.request as any).handlers[0].rejected({
        response: { status: "400" },
      });
    } catch (e) {
      expect(e.response.status).toBe("400");
    }
  });
});
