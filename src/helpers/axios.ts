import axiosVendor from "axios";
import { config } from "./config";
import { getAuthToken } from "./seafile";

let authToken = "";
const { hostname, username, password } = config().seafile;

export const axios = axiosVendor.create({
  baseURL: `${hostname}`,
  timeout: 1000,
});

axios.interceptors.request.use(
  async (config) => {
    authToken =
      authToken || (await getAuthToken({ hostname, username, password }));
    config.headers.Authorization = `Token ${authToken}`;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
