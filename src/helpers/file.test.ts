import fs from "fs";
import tmp from "tmp";
import { Readable } from "stream";
import axios from "axios";
import { logger } from "./logger";
import { deleteTmpFile, downloadTmpFile } from "./file";
import * as seafile from "./seafile";

jest.mock("./config", () => ({
  config: jest.fn(() => {
    return {
      logLevel: "warn",
      seafile: { hostname: "host", username: "user", password: "pass" },
    };
  }),
}));

describe("downloadTmpFile", () => {
  const fileBase64 =
    "/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAABAAEDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD9/KKKKAP/2Q==";
  const buffer = Buffer.from(fileBase64, "base64");
  const stream = Readable.from(buffer.toString());

  it("should download a temporary file", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const seafileGetFileLinkSpy = jest
      .spyOn(seafile, "getFileLink")
      .mockResolvedValue("link-to-file");
    const axiosGetSpy = jest
      .spyOn(axios, "get")
      .mockResolvedValue({ data: stream });

    const fileResult = await downloadTmpFile({
      libraryId: "00000000",
      path: "path/file.txt",
    });

    try {
      fs.accessSync(fileResult.name);
    } catch {
      fail("Temporary file was not downloaded");
    }

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(seafileGetFileLinkSpy).toHaveBeenCalledTimes(1);
    expect(seafileGetFileLinkSpy).toHaveBeenCalledWith({
      libraryId: "00000000",
      path: "path/file.txt",
    });
    expect(axiosGetSpy).toHaveBeenCalledTimes(1);
    expect(axiosGetSpy).toHaveBeenCalledWith("link-to-file", {
      responseType: "stream",
    });
    expect(fileResult.name).toMatch(new RegExp("^/tmp/tmp-\\d+-\\w{12}$"));

    fileResult.removeCallback();
  });
});

describe("deleteTmpFile", () => {
  it("should delete a temporary file", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const tmpFile = tmp.fileSync();

    try {
      fs.accessSync(tmpFile.name);
    } catch {
      fail("Temporary file was not created");
    }

    deleteTmpFile({ file: tmpFile });

    expect.assertions(1);
    try {
      fs.accessSync(`${tmpFile.name}`);
      fail("Temporary file was not deleted");
    } catch {
      expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    }
  });
});
