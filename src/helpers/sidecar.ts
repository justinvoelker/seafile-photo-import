import { stripExtraSlashes } from "../utils/string";
import { logger } from "./logger";
import { fileExists } from "./seafile";

import * as sidecar from "./sidecar";

export async function ensureSidecarFileExists({
  libraryId,
  primaryFileName,
  primaryFilePath,
  findPrimaryPattern,
  findSidecarReplace,
  required,
}: {
  libraryId: string;
  primaryFileName: string;
  primaryFilePath: string;
  findPrimaryPattern: string;
  findSidecarReplace: string;
  required: boolean;
}): Promise<void> {
  logger.debug(
    `sidecar.ensureSidecarFilesExist({primaryFileName: "${primaryFileName}", findPrimaryPattern: "${findPrimaryPattern}", findSidecarReplace: "${findSidecarReplace}, required: "${required}"})`
  );

  if (required) {
    const sidecarFilename = sidecar.getSidecarFilename({
      name: primaryFileName,
      find: findPrimaryPattern,
      replace: findSidecarReplace,
    });

    const sidecarExists = await fileExists({
      libraryId,
      path: `/${stripExtraSlashes(primaryFilePath)}/${sidecarFilename}`,
    });

    if (!sidecarExists) {
      throw `Unable to find sidecar file ${sidecarFilename} for primary file ${primaryFileName}`;
    }
  }
}

export function getSidecarFilename({
  name,
  find,
  replace,
}: {
  name: string;
  find: string;
  replace: string;
}): string {
  logger.debug(
    `sidecar.ensureSidecarFilesExist({name: "${name}", find: "${find}", replace: "${replace}"})`
  );

  return name.replace(new RegExp(find), replace);
}
