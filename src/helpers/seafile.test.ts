import axiosVendor from "axios";
import { logger } from "./logger";
import { axios as axiosSeafile } from "./axios";
import {
  createDirectory,
  directoryExists,
  fileExists,
  getAuthToken,
  getFileLink,
  getFiles,
  getLibraryId,
  moveFile,
  renameAndMoveFile,
  renameFile,
} from "./seafile";

jest.mock("./logger");
jest.mock("./axios");

jest.mock("./config", () => ({
  config: jest.fn(() => {
    return {
      logLevel: "warn",
      seafile: { hostname: "host", username: "user", password: "pass" },
    };
  }),
}));

describe("createDirectory", () => {
  it("should only console log action during a dry run", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerVerboseSpy = jest.spyOn(logger, "verbose").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await createDirectory({
      dryRun: true,
      libraryId: "00000000",
      libraryName: "MyLibrary",
      path: "/path",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerVerboseSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(0);
  });

  it("should execute POST request", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerVerboseSpy = jest.spyOn(logger, "verbose").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await createDirectory({
      dryRun: false,
      libraryId: "00000000",
      libraryName: "MyLibrary",
      path: "/path",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerVerboseSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledWith(
      "/api2/repos/00000000/dir/?p=/path",
      "operation=mkdir"
    );
  });
});

describe("directoryExists", () => {
  it("should return true if the directory exists", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockResolvedValue({});

    const exists = await directoryExists({
      libraryId: "00000000",
      path: "/path",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(exists).toBe(true);
    expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledWith(
      "/api/v2.1/repos/00000000/dir/detail/?path=/path"
    );
  });

  it("should return false if the directory does not exist", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockRejectedValue({
      response: { status: "404" },
    });

    const exists = await directoryExists({
      libraryId: "00000000",
      path: "/path",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(exists).toBe(false);
    expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledWith(
      "/api/v2.1/repos/00000000/dir/detail/?path=/path"
    );
  });

  it("should throw an error if an unknown error occurs", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockRejectedValue({
      response: { status: "500" },
    });

    expect.assertions(4);
    try {
      await directoryExists({
        libraryId: "00000000",
        path: "/path",
      });
    } catch (err) {
      expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
      expect(err.response.status).toBe("500");
      expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
      expect(axiosSeafile.get).toHaveBeenCalledWith(
        "/api/v2.1/repos/00000000/dir/detail/?path=/path"
      );
    }
  });
});

describe("fileExists", () => {
  it("should return true if the file exists", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockResolvedValue({});

    const exists = await fileExists({
      libraryId: "00000000",
      path: "/path/file.ext",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(exists).toBe(true);
    expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledWith(
      "/api2/repos/00000000/file/detail/?p=/path/file.ext"
    );
  });

  it("should return false if the file does not exist", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockRejectedValue({
      response: { status: "404" },
    });

    const exists = await fileExists({
      libraryId: "00000000",
      path: "/path/file.ext",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(exists).toBe(false);
    expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledWith(
      "/api2/repos/00000000/file/detail/?p=/path/file.ext"
    );
  });

  it("should throw an error if an unknown error occurs", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockRejectedValue({
      response: { status: "500" },
    });

    expect.assertions(4);
    try {
      await fileExists({
        libraryId: "00000000",
        path: "/path/file.ext",
      });
    } catch (err) {
      expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
      expect(err.response.status).toBe("500");
      expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
      expect(axiosSeafile.get).toHaveBeenCalledWith(
        "/api2/repos/00000000/file/detail/?p=/path/file.ext"
      );
    }
  });
});

describe("getAuthToken", () => {
  it("should return auth token", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    axiosVendor.post = jest.fn().mockResolvedValue({ data: { token: "0123" } });

    const token = await getAuthToken({
      hostname: "host",
      username: "user",
      password: "pass",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(token).toBe("0123");
    expect(axiosVendor.post).toHaveBeenCalledTimes(1);
    expect(axiosVendor.post).toHaveBeenCalledWith("host/api2/auth-token/", {
      username: "user",
      password: "pass",
    });
  });
});

describe("getFileLink", () => {
  it("should return file link", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockResolvedValue({ data: {} });

    await getFileLink({
      libraryId: "00000000",
      path: "path/to/file.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledWith(
      `/api2/repos/00000000/file?p=path/to/file.jpg`
    );
  });
});

describe("getFiles", () => {
  it("should return single directory of files by default", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerVerboseSpy = jest.spyOn(logger, "verbose").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockResolvedValue({ data: {} });

    await getFiles({
      libraryId: "00000000",
      libraryName: "Src Library",
      path: "/path",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerVerboseSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledWith(
      "/api2/repos/00000000/dir?p=/path&t=f&recursive=0"
    );
  });

  it("should return single directory of files", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerVerboseSpy = jest.spyOn(logger, "verbose").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockResolvedValue({ data: {} });

    await getFiles({
      libraryId: "00000000",
      libraryName: "Src Library",
      path: "/path",
      recursive: false,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerVerboseSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledWith(
      "/api2/repos/00000000/dir?p=/path&t=f&recursive=0"
    );
  });

  it("should return multiple directories of files", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerVerboseSpy = jest.spyOn(logger, "verbose").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockResolvedValue({ data: {} });

    await getFiles({
      libraryId: "00000000",
      libraryName: "Src Library",
      path: "/path",
      recursive: true,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerVerboseSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledWith(
      "/api2/repos/00000000/dir?p=/path&t=f&recursive=1"
    );
  });
});

describe("getLibraryId", () => {
  it("should throw error if library does not exist", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockResolvedValue({
      data: [{ id: "00000000", name: "Library Name" }],
    });

    expect.assertions(4);
    try {
      await getLibraryId({ name: "Incorrect Name" });
    } catch (err) {
      expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
      expect(err).toContain("Incorrect Name");
      expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
      expect(axiosSeafile.get).toHaveBeenCalledWith("/api2/repos?type=mine");
    }
  });

  it("should return library ID if library exists", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    (axiosSeafile.get as jest.Mock).mockResolvedValue({
      data: [{ id: "00000000", name: "Library Name" }],
    });

    const libraryId = await getLibraryId({
      name: "Library Name",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(libraryId).toBe("00000000");
    expect(axiosSeafile.get).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.get).toHaveBeenCalledWith("/api2/repos?type=mine");
  });
});

describe("moveFile", () => {
  it("should do nothing if directories match", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await moveFile({
      dryRun: true,
      srcLibraryId: "00000000",
      srcLibraryName: "Src Library",
      srcFilePath: "/Path/To/SameDir/OldFileName.jpg",
      dstLibraryId: "00000001",
      dstLibraryName: "Dst Library",
      dstFilePath: "/Path/To/SameDir/NewFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(0);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(0);
  });

  it("should only console log action during a dry run", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await moveFile({
      dryRun: true,
      srcLibraryId: "00000000",
      srcLibraryName: "Src Library",
      srcFilePath: "/Path/To/OldDir/OldFileName.jpg",
      dstLibraryId: "00000001",
      dstLibraryName: "Dst Library",
      dstFilePath: "/Path/To/NewDir/NewFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(0);
  });

  it("should execute POST request", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await moveFile({
      dryRun: false,
      srcLibraryId: "00000000",
      srcLibraryName: "Src Library",
      srcFilePath: "/Path/To/OldDir/OldFileName.jpg",
      dstLibraryId: "00000001",
      dstLibraryName: "Dst Library",
      dstFilePath: "/Path/To/NewDir/NewFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledWith(
      "/api/v2.1/repos/00000000/file/?p=/Path/To/OldDir/OldFileName.jpg",
      "operation=move&dst_repo=00000001&dst_dir=/Path/To/NewDir"
    );
  });
});

describe("renameAndMoveFile", () => {
  it("should do nothing if libraries match, directories match, and file names match", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await renameAndMoveFile({
      dryRun: false,
      srcLibraryId: "00000000",
      srcLibraryName: "Library",
      srcFilePath: "/Path/To/Dir/FileName.jpg",
      dstLibraryId: "00000000",
      dstLibraryName: "Library",
      dstFilePath: "/Path/To/Dir/FileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(0);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(0);
  });

  it("should rename if libraries match, directories match, and file names differ", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await renameAndMoveFile({
      dryRun: false,
      srcLibraryId: "00000000",
      srcLibraryName: "Library",
      srcFilePath: "/Path/To/Dir/SrcFileName.jpg",
      dstLibraryId: "00000000",
      dstLibraryName: "Library",
      dstFilePath: "/Path/To/Dir/DstFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenNthCalledWith(
      1,
      "/api/v2.1/repos/00000000/file/?p=/Path/To/Dir/SrcFileName.jpg",
      "operation=rename&newname=DstFileName.jpg"
    );
  });

  it("should move if libraries match, directories differ, and file names match", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await renameAndMoveFile({
      dryRun: false,
      srcLibraryId: "00000000",
      srcLibraryName: "Library",
      srcFilePath: "/Path/To/SrcDir/FileName.jpg",
      dstLibraryId: "00000000",
      dstLibraryName: "Library",
      dstFilePath: "/Path/To/DstDir/FileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenNthCalledWith(
      1,
      "/api/v2.1/repos/00000000/file/?p=/Path/To/SrcDir/FileName.jpg",
      "operation=move&dst_repo=00000000&dst_dir=/Path/To/DstDir"
    );
  });

  it("should rename and move if libraries match, directories differ, and file names differ", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await renameAndMoveFile({
      dryRun: false,
      srcLibraryId: "00000000",
      srcLibraryName: "Library",
      srcFilePath: "/Path/To/SrcDir/SrcFileName.jpg",
      dstLibraryId: "00000000",
      dstLibraryName: "Library",
      dstFilePath: "/Path/To/DstDir/DstFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(2);
    expect(axiosSeafile.post).toHaveBeenNthCalledWith(
      1,
      "/api/v2.1/repos/00000000/file/?p=/Path/To/SrcDir/SrcFileName.jpg",
      "operation=rename&newname=DstFileName.jpg"
    );
    expect(axiosSeafile.post).toHaveBeenNthCalledWith(
      2,
      "/api/v2.1/repos/00000000/file/?p=/Path/To/SrcDir/DstFileName.jpg",
      "operation=move&dst_repo=00000000&dst_dir=/Path/To/DstDir"
    );
  });

  it("should move if libraries differ, directories match, and file names match", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await renameAndMoveFile({
      dryRun: false,
      srcLibraryId: "00000000",
      srcLibraryName: "Src Library",
      srcFilePath: "/Path/To/Dir/FileName.jpg",
      dstLibraryId: "00000001",
      dstLibraryName: "Dst Library",
      dstFilePath: "/Path/To/Dir/FileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenNthCalledWith(
      1,
      "/api/v2.1/repos/00000000/file/?p=/Path/To/Dir/FileName.jpg",
      "operation=move&dst_repo=00000001&dst_dir=/Path/To/Dir"
    );
  });

  it("should rename and move if libraries differ, directories match, and file names differ", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await renameAndMoveFile({
      dryRun: false,
      srcLibraryId: "00000000",
      srcLibraryName: "Src Library",
      srcFilePath: "/Path/To/Dir/SrcFileName.jpg",
      dstLibraryId: "00000001",
      dstLibraryName: "Dst Library",
      dstFilePath: "/Path/To/Dir/DstFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(2);
    expect(axiosSeafile.post).toHaveBeenNthCalledWith(
      1,
      "/api/v2.1/repos/00000000/file/?p=/Path/To/Dir/SrcFileName.jpg",
      "operation=rename&newname=DstFileName.jpg"
    );
    expect(axiosSeafile.post).toHaveBeenNthCalledWith(
      2,
      "/api/v2.1/repos/00000000/file/?p=/Path/To/Dir/DstFileName.jpg",
      "operation=move&dst_repo=00000001&dst_dir=/Path/To/Dir"
    );
  });

  it("should move if libraries differ, directories differ, and file names match", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await renameAndMoveFile({
      dryRun: false,
      srcLibraryId: "00000000",
      srcLibraryName: "Src Library",
      srcFilePath: "/Path/To/SrcDir/FileName.jpg",
      dstLibraryId: "00000001",
      dstLibraryName: "Dst Library",
      dstFilePath: "/Path/To/DstDir/FileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenNthCalledWith(
      1,
      "/api/v2.1/repos/00000000/file/?p=/Path/To/SrcDir/FileName.jpg",
      "operation=move&dst_repo=00000001&dst_dir=/Path/To/DstDir"
    );
  });

  it("should rename and move if libraries differ, directories differ, and file names differ", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await renameAndMoveFile({
      dryRun: false,
      srcLibraryId: "00000000",
      srcLibraryName: "Src Library",
      srcFilePath: "/Path/To/SrcDir/SrcFileName.jpg",
      dstLibraryId: "00000001",
      dstLibraryName: "Dst Library",
      dstFilePath: "/Path/To/DstDir/DstFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(2);
    expect(axiosSeafile.post).toHaveBeenNthCalledWith(
      1,
      "/api/v2.1/repos/00000000/file/?p=/Path/To/SrcDir/SrcFileName.jpg",
      "operation=rename&newname=DstFileName.jpg"
    );
    expect(axiosSeafile.post).toHaveBeenNthCalledWith(
      2,
      "/api/v2.1/repos/00000000/file/?p=/Path/To/SrcDir/DstFileName.jpg",
      "operation=move&dst_repo=00000001&dst_dir=/Path/To/DstDir"
    );
  });

  it("should only log action during a dry run", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    await renameAndMoveFile({
      dryRun: true,
      srcLibraryId: "00000000",
      srcLibraryName: "Src Library",
      srcFilePath: "/Path/To/SrcDir/SrcFileName.jpg",
      dstLibraryId: "00000001",
      dstLibraryName: "Dst Library",
      dstFilePath: "/Path/To/DstDir/DstFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledTimes(0);
  });
});

describe("renameFile", () => {
  it("should do nothing if file names match", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    const dstFileName = await renameFile({
      dryRun: true,
      libraryId: "00000000",
      libraryName: "MyLibrary",
      path: "/Path/To/FileName.jpg",
      name: "FileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(dstFileName).toBe("FileName.jpg");
    expect(axiosSeafile.post).toHaveBeenCalledTimes(0);
  });

  it("should only console log action during a dry run", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    const dstFileName = await renameFile({
      dryRun: true,
      libraryId: "00000000",
      libraryName: "MyLibrary",
      path: "/Path/To/OldFileName.jpg",
      name: "NewFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(dstFileName).toBe("NewFileName.jpg");
    expect(axiosSeafile.post).toHaveBeenCalledTimes(0);
  });

  it("should execute POST request to rename", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const loggerInfoSpy = jest.spyOn(logger, "info").mockImplementation();
    (axiosSeafile.post as jest.Mock).mockImplementation();

    const dstFileName = await renameFile({
      dryRun: false,
      libraryId: "00000000",
      libraryName: "MyLibrary",
      path: "/Path/To/OldFileName.jpg",
      name: "NewFileName.jpg",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(loggerInfoSpy).toHaveBeenCalledTimes(1);
    expect(dstFileName).toBe("NewFileName.jpg");
    expect(axiosSeafile.post).toHaveBeenCalledTimes(1);
    expect(axiosSeafile.post).toHaveBeenCalledWith(
      "/api/v2.1/repos/00000000/file/?p=/Path/To/OldFileName.jpg",
      "operation=rename&newname=NewFileName.jpg"
    );
  });
});
