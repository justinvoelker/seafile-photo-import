import fs from "fs";
import { logger, logEdits } from "./logger";

jest.mock("./config", () => ({
  config: jest.fn(() => {
    return {
      logLevel: "warn",
    };
  }),
}));

describe("Logger instance", () => {
  it("should use log level from config", async () => {
    expect(logger.level).toBe("warn");
  });
});

describe("logEdits", () => {
  it("should write edits to log file", async () => {
    const fsAppendFileSyncSpy = jest
      .spyOn(fs, "appendFileSync")
      .mockImplementation();
    const dateAppendFileSyncSpy = jest
      .spyOn(global.Date.prototype, "toISOString")
      .mockReturnValue("2021-06-05T02:38:08.069Z");

    const year = new Date().getFullYear();

    logEdits("rename", "oldName", "newName", false);

    expect(fsAppendFileSyncSpy).toHaveBeenCalledTimes(1);
    expect(dateAppendFileSyncSpy).toHaveBeenCalledTimes(1);
    expect(fsAppendFileSyncSpy).toHaveBeenCalledWith(
      `/var/log/edits-${year}.log`,
      JSON.stringify({
        time: "2021-06-05T02:38:08.069Z",
        action: "rename",
        from: "oldName",
        to: "newName",
      }) + "\n"
    );
  });

  it("should write dryrun edits to dryrun log file", async () => {
    const fsAppendFileSyncSpy = jest
      .spyOn(fs, "appendFileSync")
      .mockImplementation();
    const dateAppendFileSyncSpy = jest
      .spyOn(global.Date.prototype, "toISOString")
      .mockReturnValue("2021-06-05T02:38:08.069Z");

    const year = new Date().getFullYear();

    logEdits("move", "oldPath", "newPath", true);

    expect(fsAppendFileSyncSpy).toHaveBeenCalledTimes(1);
    expect(dateAppendFileSyncSpy).toHaveBeenCalledTimes(1);
    expect(fsAppendFileSyncSpy).toHaveBeenCalledWith(
      `/var/log/edits-${year}.dryrun.log`,
      JSON.stringify({
        time: "2021-06-05T02:38:08.069Z",
        action: "move",
        from: "oldPath",
        to: "newPath",
      }) + "\n"
    );
  });
});
