export interface Config {
  logLevel: string;
  intervalSeconds: number;
  seafile: {
    username: string;
    password: string;
    hostname: string;
  };
  tasks: Task[];
}

interface Sidecar {
  name: string;
  findPrimaryPattern: string;
  findSidecarReplace: string;
  namePrimaryPattern: string;
  nameSidecarReplace: string;
  required: boolean;
}

interface Task {
  name: string;
  enabled: boolean;
  dryRun: boolean;
  logLevel?: string;
  metadataModifiers: string[];
  srcDirectory: string;
  srcFilenamePattern: string;
  srcFilenameDateTimePattern?: string;
  srcFilenameDateTimeReplace?: string;
  srcFilenameDateTimeTimezoneParse?: string;
  srcFilenameDateTimeTimezoneLocal?: string;
  dstDirectory: string;
  dstFilenameExtensionCase?: string;
  dstFilenameExtensionCasePattern: string;
  steps: TaskSteps[];
  sidecars: Sidecar[];
}

interface TaskSteps {
  name: string;
  find: string;
  repl: string;
  flag?: string;
}

export function getEnvVarKeys(regex: RegExp): string[] {
  const keys: string[] = [];
  Object.keys(process.env).forEach((key) => {
    const match = regex.exec(key);
    if (match) keys.push(match[1]);
  });
  return keys.sort((a, b) => parseInt(a) - parseInt(b));
}

export function getEnvVarReq(key: string): string {
  if (!(key in process.env))
    throw `Missing required environment variable ${key}`;
  return process.env[key] as string;
}

export function getEnvVarOpt(key: string): string | undefined {
  return process.env[key];
}

export function getEnvVarBoolean(key: string, defaultVal: boolean): boolean {
  if (!(key in process.env)) return defaultVal;
  return (process.env[key] as string).toLowerCase() == "true" || false;
}

export function config(): Config {
  const taskKeys = getEnvVarKeys(new RegExp(`TASK(\\d+)_NAME`));
  const tasks: Array<Task> = [];

  for (const taskKey of taskKeys) {
    const sidecarKeys = getEnvVarKeys(
      new RegExp(`TASK${taskKey}_SIDECAR(\\d+)_NAME$`)
    );
    const sidecars: Array<Sidecar> = [];

    for (const sidecarKey of sidecarKeys) {
      const sidecarPrefix = `TASK${taskKey}_SIDECAR${sidecarKey}`;
      const sidecar = {
        name: getEnvVarReq(`${sidecarPrefix}_NAME`),
        findPrimaryPattern:
          getEnvVarOpt(`${sidecarPrefix}_FIND_PRIMARY_PATTERN`) || "(.*)\\..*",
        findSidecarReplace: getEnvVarReq(
          `${sidecarPrefix}_FIND_SIDECAR_REPLACE`
        ),
        namePrimaryPattern:
          getEnvVarOpt(`${sidecarPrefix}_NAME_PRIMARY_PATTERN`) || "(.*)\\..*",
        nameSidecarReplace: getEnvVarReq(
          `${sidecarPrefix}_NAME_SIDECAR_REPLACE`
        ),
        required: getEnvVarBoolean(`${sidecarPrefix}_REQUIRED`, false),
      };
      sidecars.push(sidecar);
    }
    const stepKeys = getEnvVarKeys(
      new RegExp(`TASK${taskKey}_DST_FILENAME_STEP(\\d+)_NAME`)
    );
    const steps: Array<TaskSteps> = [];

    for (const stepKey of stepKeys) {
      const stepPrefix = `TASK${taskKey}_DST_FILENAME_STEP${stepKey}`;
      const step = {
        name: getEnvVarReq(`${stepPrefix}_NAME`),
        find: getEnvVarReq(`${stepPrefix}_FIND`),
        repl: getEnvVarReq(`${stepPrefix}_REPL`),
        flag: getEnvVarOpt(`${stepPrefix}_FLAG`),
      };
      steps.push(step);
    }

    const taskPrefix = `TASK${taskKey}`;
    const task: Task = {
      name: getEnvVarReq(`${taskPrefix}_NAME`),
      enabled: getEnvVarBoolean(`${taskPrefix}_ENABLED`, true),
      dryRun: getEnvVarBoolean(`${taskPrefix}_DRY_RUN`, false),
      logLevel: getEnvVarOpt(`${taskPrefix}_LOG_LEVEL`),
      metadataModifiers:
        getEnvVarOpt(`${taskPrefix}_METADATA_MODIFIERS`)?.split(",") || [],
      srcDirectory: getEnvVarReq(`${taskPrefix}_SRC_DIRECTORY`),
      srcFilenamePattern:
        getEnvVarOpt(`${taskPrefix}_SRC_FILENAME_PATTERN`) || ".*",
      srcFilenameDateTimePattern: getEnvVarOpt(
        `${taskPrefix}_SRC_FILENAME_DATETIME_PATTERN`
      ),
      srcFilenameDateTimeReplace: getEnvVarOpt(
        `${taskPrefix}_SRC_FILENAME_DATETIME_REPLACE`
      ),
      srcFilenameDateTimeTimezoneParse: getEnvVarOpt(
        `${taskPrefix}_SRC_FILENAME_DATETIME_TIMEZONE_PARSE`
      ),
      srcFilenameDateTimeTimezoneLocal: getEnvVarOpt(
        `${taskPrefix}_SRC_FILENAME_DATETIME_TIMEZONE_LOCAL`
      ),
      dstDirectory: getEnvVarReq(`${taskPrefix}_DST_DIRECTORY`),
      dstFilenameExtensionCase: getEnvVarOpt(
        `${taskPrefix}_DST_FILENAME_EXTENSION_CASE`
      )?.toLowerCase(),
      dstFilenameExtensionCasePattern:
        getEnvVarOpt(`${taskPrefix}_DST_FILENAME_EXTENSION_CASE_PATTERN`) ||
        ".*\\.(.*)",
      sidecars,
      steps,
    };
    tasks.push(task);
  }

  return {
    logLevel: getEnvVarOpt("LOG_LEVEL") || "warn",
    intervalSeconds: parseInt(getEnvVarOpt("INTERVAL_SECONDS") || "3600"),
    seafile: {
      username: getEnvVarReq("SEAFILE_USERNAME"),
      password: getEnvVarReq("SEAFILE_PASSWORD"),
      hostname: getEnvVarReq("SEAFILE_HOSTNAME"),
    },
    tasks,
  };
}
