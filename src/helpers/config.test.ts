import {
  config,
  getEnvVarKeys,
  getEnvVarOpt,
  getEnvVarReq,
  getEnvVarBoolean,
} from "./config";

beforeEach(() => {
  Object.keys(process.env).forEach((key) => {
    const match = new RegExp(`TASK(\\d+)_`).test(key);
    if (match) delete process.env[key];
  });
  process.env.SEAFILE_USERNAME = "seafile_user";
  process.env.SEAFILE_PASSWORD = "seafile_pass";
  process.env.SEAFILE_HOSTNAME = "seafile_host";
});

describe("getEnvVarKeys", () => {
  it("should return numerically ordered array of strings", () => {
    process.env.TEST03_NAME = "";
    process.env.TEST1_NAME = "";
    process.env.TEST10_NAME = "";
    process.env.TEST0_NAME = "";
    process.env.TEST2_NAME = "";

    const keys = getEnvVarKeys(/TEST(\d+)_NAME/);

    expect(keys[0]).toBe("0");
    expect(keys[1]).toBe("1");
    expect(keys[2]).toBe("2");
    expect(keys[3]).toBe("03");
    expect(keys[4]).toBe("10");
  });
});

describe("getEnvVarReq", () => {
  it("should return string value if required env var exists", () => {
    process.env.TEST_ENV_VAR = "testVal";

    const value = getEnvVarReq("TEST_ENV_VAR");

    expect(value).toBe("testVal");
  });

  it("should throw error if required env var does not exist", () => {
    delete process.env.TEST_ENV_VAR;

    expect(() => {
      getEnvVarReq("TEST_ENV_VAR");
    }).toThrow();
  });
});

describe("getEnvVarOpt", () => {
  it("should return string value if optional env var exists", () => {
    process.env.TEST_ENV_VAR = "testVal";

    const value = getEnvVarOpt("TEST_ENV_VAR");

    expect(value).toBe("testVal");
  });

  it("should return undefined if optional env var does not exist", () => {
    delete process.env.TEST_ENV_VAR;

    const value = getEnvVarOpt("TEST_ENV_VAR");

    expect(value).toBe(undefined);
  });
});

describe("getEnvVarBoolean", () => {
  it.each([
    ["true", true, true],
    ["true", false, true],
    ["false", true, false],
    ["false", false, false],
    [null, true, true],
    [null, false, false],
  ])(
    "should return boolean for env var value of %s and default value of %i",
    (envVar, defaultVal, expected) => {
      envVar != null
        ? (process.env.TEST_ENV_VAR = envVar)
        : delete process.env.TEST_ENV_VAR;

      const value = getEnvVarBoolean("TEST_ENV_VAR", defaultVal);

      expect(value).toBe(expected);
    }
  );
});

describe("getConfig", () => {
  it("should define default log level if env var does not exist", () => {
    delete process.env.LOG_LEVEL;

    const { logLevel } = config();

    expect(logLevel).toBe("warn");
  });

  it("should parse interval seconds if env var exists", () => {
    process.env.LOG_LEVEL = "error";

    const { logLevel } = config();

    expect(logLevel).toBe("error");
  });

  it("should define default interval seconds if env var does not exist", () => {
    delete process.env.INTERVAL_SECONDS;

    const { intervalSeconds } = config();

    expect(intervalSeconds).toBe(3600);
  });

  it("should parse interval seconds if env var exists", () => {
    process.env.INTERVAL_SECONDS = "5";

    const { intervalSeconds } = config();

    expect(intervalSeconds).toBe(5);
  });

  it("should parse environment variables for seafile", () => {
    process.env.SEAFILE_USERNAME = "user";
    process.env.SEAFILE_PASSWORD = "pass";
    process.env.SEAFILE_HOSTNAME = "host";

    const { seafile } = config();

    expect(seafile.username).toBe("user");
    expect(seafile.password).toBe("pass");
    expect(seafile.hostname).toBe("host");
  });

  it("should parse environment variables for multiple tasks", () => {
    process.env.TASK0_NAME = "Task 0 name";
    process.env.TASK0_ENABLED = "FALSE";
    process.env.TASK0_DRY_RUN = "TRUE";
    process.env.TASK0_LOG_LEVEL = "warn";
    process.env.TASK0_METADATA_MODIFIERS =
      "Task 0 Metadata Modifier 0,Task 0 Metadata Modifier 1";
    process.env.TASK0_SRC_DIRECTORY = "Task 0 Src Directory";
    process.env.TASK0_SRC_FILENAME_PATTERN = "Task 0 Src Pattern";
    process.env.TASK0_SRC_FILENAME_DATETIME_PATTERN =
      "Task 0 Src Filename DateTime Pattern";
    process.env.TASK0_SRC_FILENAME_DATETIME_REPLACE =
      "Task 0 Src Filename DateTime Replace";
    process.env.TASK0_SRC_FILENAME_DATETIME_TIMEZONE_PARSE =
      "Task 0 Src Filename DateTime Timezone Parse";
    process.env.TASK0_SRC_FILENAME_DATETIME_TIMEZONE_LOCAL =
      "Task 0 Src Filename DateTime Timezone Local";
    process.env.TASK0_DST_DIRECTORY = "Task 0 Dst Directory";
    process.env.TASK0_DST_FILENAME_EXTENSION_CASE =
      "Task 0 Filename Extension Case";
    process.env.TASK0_DST_FILENAME_EXTENSION_CASE_PATTERN =
      "Task 0 Filename Extension Case Pattern";
    process.env.TASK0_DST_FILENAME_STEP0_NAME = "Task 0 Step 0 Name";
    process.env.TASK0_DST_FILENAME_STEP0_FIND = "Task 0 Step 0 Find";
    process.env.TASK0_DST_FILENAME_STEP0_REPL = "Task 0 Step 0 Repl";
    process.env.TASK0_DST_FILENAME_STEP0_FLAG = "Task 0 Step 0 Flag";
    process.env.TASK0_DST_FILENAME_STEP1_NAME = "Task 0 Step 1 Name";
    process.env.TASK0_DST_FILENAME_STEP1_FIND = "Task 0 Step 1 Find";
    process.env.TASK0_DST_FILENAME_STEP1_REPL = "Task 0 Step 1 Repl";
    process.env.TASK0_SIDECAR0_NAME = "Task 0 Sidecar 0 Name";
    process.env.TASK0_SIDECAR0_FIND_PRIMARY_PATTERN =
      "Task 0 Sidecar 0 Find Primary Pattern";
    process.env.TASK0_SIDECAR0_FIND_SIDECAR_REPLACE =
      "Task 0 Sidecar 0 Find Sidecar Replace";
    process.env.TASK0_SIDECAR0_NAME_PRIMARY_PATTERN =
      "Task 0 Sidecar 0 Name Primary Pattern";
    process.env.TASK0_SIDECAR0_NAME_SIDECAR_REPLACE =
      "Task 0 Sidecar 0 Name Sidecar Replace";
    process.env.TASK0_SIDECAR0_REQUIRED = "TRUE";
    process.env.TASK0_SIDECAR1_NAME = "Task 0 Sidecar 1 Name";
    process.env.TASK0_SIDECAR1_FIND_PRIMARY_PATTERN =
      "Task 0 Sidecar 1 Find Primary Pattern";
    process.env.TASK0_SIDECAR1_FIND_SIDECAR_REPLACE =
      "Task 0 Sidecar 1 Find Sidecar Replace";
    process.env.TASK0_SIDECAR1_NAME_PRIMARY_PATTERN =
      "Task 0 Sidecar 1 Name Primary Pattern";
    process.env.TASK0_SIDECAR1_NAME_SIDECAR_REPLACE =
      "Task 0 Sidecar 1 Name Sidecar Replace";
    process.env.TASK0_SIDECAR1_REQUIRED = "FALSE";
    process.env.TASK1_NAME = "Task 1 name";
    process.env.TASK1_SRC_DIRECTORY = "Task 1 Src Directory";
    process.env.TASK1_DST_DIRECTORY = "Task 1 Dst Directory";
    process.env.TASK1_DST_FILENAME_STEP0_NAME = "Task 1 Step 0 Name";
    process.env.TASK1_DST_FILENAME_STEP0_FIND = "Task 1 Step 0 Find";
    process.env.TASK1_DST_FILENAME_STEP0_REPL = "Task 1 Step 0 Repl";
    process.env.TASK1_DST_FILENAME_STEP0_FLAG = "Task 1 Step 0 Flag";
    process.env.TASK1_DST_FILENAME_STEP1_NAME = "Task 1 Step 1 Name";
    process.env.TASK1_DST_FILENAME_STEP1_FIND = "Task 1 Step 1 Find";
    process.env.TASK1_DST_FILENAME_STEP1_REPL = "Task 1 Step 1 Repl";
    process.env.TASK1_SIDECAR0_NAME = "Task 1 Sidecar 0 Name";
    process.env.TASK1_SIDECAR0_FIND_SIDECAR_REPLACE =
      "Task 1 Sidecar 0 Find Sidecar Replace";
    process.env.TASK1_SIDECAR0_NAME_SIDECAR_REPLACE =
      "Task 1 Sidecar 0 Name Sidecar Replace";
    process.env.TASK1_SIDECAR1_NAME = "Task 1 Sidecar 1 Name";
    process.env.TASK1_SIDECAR1_FIND_SIDECAR_REPLACE =
      "Task 1 Sidecar 1 Find Sidecar Replace";
    process.env.TASK1_SIDECAR1_NAME_SIDECAR_REPLACE =
      "Task 1 Sidecar 1 Name Sidecar Replace";

    const { tasks } = config();

    expect(tasks[0].name).toBe("Task 0 name");
    expect(tasks[0].enabled).toBe(false);
    expect(tasks[0].dryRun).toBe(true);
    expect(tasks[0].logLevel).toBe("warn");
    expect(tasks[0].metadataModifiers).toContain("Task 0 Metadata Modifier 0");
    expect(tasks[0].metadataModifiers).toContain("Task 0 Metadata Modifier 1");
    expect(tasks[0].srcDirectory).toBe("Task 0 Src Directory");
    expect(tasks[0].srcFilenamePattern).toBe("Task 0 Src Pattern");
    expect(tasks[0].srcFilenameDateTimePattern).toBe(
      "Task 0 Src Filename DateTime Pattern"
    );
    expect(tasks[0].srcFilenameDateTimeReplace).toBe(
      "Task 0 Src Filename DateTime Replace"
    );
    expect(tasks[0].srcFilenameDateTimeTimezoneParse).toBe(
      "Task 0 Src Filename DateTime Timezone Parse"
    );
    expect(tasks[0].srcFilenameDateTimeTimezoneLocal).toBe(
      "Task 0 Src Filename DateTime Timezone Local"
    );
    expect(tasks[0].dstDirectory).toBe("Task 0 Dst Directory");
    expect(tasks[0].dstFilenameExtensionCase).toBe(
      "task 0 filename extension case"
    );
    expect(tasks[0].dstFilenameExtensionCasePattern).toBe(
      "Task 0 Filename Extension Case Pattern"
    );
    expect(tasks[0].steps[0].name).toBe("Task 0 Step 0 Name");
    expect(tasks[0].steps[0].find).toBe("Task 0 Step 0 Find");
    expect(tasks[0].steps[0].repl).toBe("Task 0 Step 0 Repl");
    expect(tasks[0].steps[0].flag).toBe("Task 0 Step 0 Flag");
    expect(tasks[0].steps[1].name).toBe("Task 0 Step 1 Name");
    expect(tasks[0].steps[1].find).toBe("Task 0 Step 1 Find");
    expect(tasks[0].steps[1].repl).toBe("Task 0 Step 1 Repl");
    expect(tasks[0].sidecars[0].name).toBe("Task 0 Sidecar 0 Name");
    expect(tasks[0].sidecars[0].findPrimaryPattern).toBe(
      "Task 0 Sidecar 0 Find Primary Pattern"
    );
    expect(tasks[0].sidecars[0].findSidecarReplace).toBe(
      "Task 0 Sidecar 0 Find Sidecar Replace"
    );
    expect(tasks[0].sidecars[0].namePrimaryPattern).toBe(
      "Task 0 Sidecar 0 Name Primary Pattern"
    );
    expect(tasks[0].sidecars[0].nameSidecarReplace).toBe(
      "Task 0 Sidecar 0 Name Sidecar Replace"
    );
    expect(tasks[0].sidecars[0].required).toBe(true);
    expect(tasks[0].sidecars[1].name).toBe("Task 0 Sidecar 1 Name");
    expect(tasks[0].sidecars[1].findPrimaryPattern).toBe(
      "Task 0 Sidecar 1 Find Primary Pattern"
    );
    expect(tasks[0].sidecars[1].findSidecarReplace).toBe(
      "Task 0 Sidecar 1 Find Sidecar Replace"
    );
    expect(tasks[0].sidecars[1].namePrimaryPattern).toBe(
      "Task 0 Sidecar 1 Name Primary Pattern"
    );
    expect(tasks[0].sidecars[1].nameSidecarReplace).toBe(
      "Task 0 Sidecar 1 Name Sidecar Replace"
    );
    expect(tasks[0].sidecars[1].required).toBe(false);
    expect(tasks[0].steps[1].flag).toBeUndefined();
    expect(tasks[1].name).toBe("Task 1 name");
    expect(tasks[1].enabled).toBe(true);
    expect(tasks[1].dryRun).toBe(false);
    expect(tasks[1].metadataModifiers).toStrictEqual([]);
    expect(tasks[1].srcDirectory).toBe("Task 1 Src Directory");
    expect(tasks[1].srcFilenamePattern).toBe(".*");
    expect(tasks[1].srcFilenameDateTimePattern).toBeUndefined();
    expect(tasks[1].srcFilenameDateTimeReplace).toBeUndefined();
    expect(tasks[1].srcFilenameDateTimeTimezoneParse).toBeUndefined();
    expect(tasks[1].srcFilenameDateTimeTimezoneLocal).toBeUndefined();
    expect(tasks[1].dstDirectory).toBe("Task 1 Dst Directory");
    expect(tasks[1].dstFilenameExtensionCase).toBeUndefined();
    expect(tasks[1].dstFilenameExtensionCasePattern).toBe(".*\\.(.*)");
    expect(tasks[1].steps[0].name).toBe("Task 1 Step 0 Name");
    expect(tasks[1].steps[0].find).toBe("Task 1 Step 0 Find");
    expect(tasks[1].steps[0].repl).toBe("Task 1 Step 0 Repl");
    expect(tasks[1].steps[0].flag).toBe("Task 1 Step 0 Flag");
    expect(tasks[1].steps[1].name).toBe("Task 1 Step 1 Name");
    expect(tasks[1].steps[1].find).toBe("Task 1 Step 1 Find");
    expect(tasks[1].steps[1].repl).toBe("Task 1 Step 1 Repl");
    expect(tasks[1].steps[1].flag).toBeUndefined();
    expect(tasks[1].sidecars[0].name).toBe("Task 1 Sidecar 0 Name");
    expect(tasks[1].sidecars[0].findPrimaryPattern).toBe("(.*)\\..*");
    expect(tasks[1].sidecars[0].findSidecarReplace).toBe(
      "Task 1 Sidecar 0 Find Sidecar Replace"
    );
    expect(tasks[1].sidecars[0].namePrimaryPattern).toBe("(.*)\\..*");
    expect(tasks[1].sidecars[0].nameSidecarReplace).toBe(
      "Task 1 Sidecar 0 Name Sidecar Replace"
    );
    expect(tasks[1].sidecars[0].required).toBe(false);
    expect(tasks[1].sidecars[1].name).toBe("Task 1 Sidecar 1 Name");
    expect(tasks[1].sidecars[1].findPrimaryPattern).toBe("(.*)\\..*");
    expect(tasks[1].sidecars[1].findSidecarReplace).toBe(
      "Task 1 Sidecar 1 Find Sidecar Replace"
    );
    expect(tasks[1].sidecars[1].namePrimaryPattern).toBe("(.*)\\..*");
    expect(tasks[1].sidecars[1].nameSidecarReplace).toBe(
      "Task 1 Sidecar 1 Name Sidecar Replace"
    );
    expect(tasks[1].sidecars[1].required).toBe(false);
  });
});
