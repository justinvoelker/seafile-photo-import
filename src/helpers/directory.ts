import { logger } from "./logger";
import { createDirectory, directoryExists } from "./seafile";

export async function createDirectoryHierarchy({
  dryRun,
  libraryId,
  libraryName,
  directory,
}: {
  dryRun: boolean;
  libraryId: string;
  libraryName: string;
  directory: string;
}): Promise<void> {
  logger.debug(
    `directory.createDirectoryHierarchy({dryRun: ${dryRun}, libraryId: "${libraryId}", libraryName: "${libraryName}", directory: "${directory}"})`
  );
  if (directory == "/") return;
  const levels = directory.split("/").length;
  for (let i = 1; i <= levels; i++) {
    const path = `/${directory.split("/").slice(1, i).join("/")}`;
    if (path == "/") continue;
    const exists = await directoryExists({ libraryId, path });
    if (!exists) {
      await createDirectory({
        dryRun,
        libraryId,
        libraryName,
        path,
      });
    }
  }
}
