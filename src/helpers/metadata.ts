import { exiftool, Tags, ExifDateTime } from "exiftool-vendored";
import { DateTime } from "luxon";

import { logger } from "./logger";
import { downloadTmpFile } from "./file";

import * as metadata from "./metadata";

export async function getMetadataExifTool({
  libraryId,
  path,
}: {
  libraryId: string;
  path: string;
}): Promise<Tags> {
  logger.debug(
    `metadata.getMetadataExifTool({libraryId: "${libraryId}", path: "${path}"})`
  );

  const tmpFile = await downloadTmpFile({ libraryId, path });

  const exifToolTags = await exiftool.read(tmpFile.name);

  tmpFile.removeCallback();

  return exifToolTags;
}

export function getMetadataExtendedDates({
  exifToolTags,
  metadataModifiers,
}: {
  exifToolTags: Tags;
  metadataModifiers: string[];
}): Record<string, unknown> {
  logger.debug(
    `metadata.getMetadataExtended({exifToolTags: "OMITTED FOR LOGGING"})`
  );

  let tags = {};

  Object.keys(exifToolTags).forEach((key) => {
    const exifTagValue = (exifToolTags as Record<string, unknown>)[key];
    if (
      exifTagValue &&
      Object.prototype.hasOwnProperty.call(exifTagValue, "tzoffsetMinutes")
    ) {
      const exifDateValue = exifTagValue as ExifDateTime;
      const utcExt = new Date(String(exifDateValue)).toISOString();

      let keyTags = {};
      if (exifDateValue.tzoffsetMinutes !== undefined) {
        const dateTime = new Date(String(exifDateValue)).getTime();
        const dateOffset =
          (exifDateValue.tzoffsetMinutes as number) * 60 * 1000;

        const offsetMultiplier = metadataModifiers.includes(
          "DATE_RAW_VALUE_UTC_AS_LOCAL"
        )
          ? 2
          : 1;
        const localExt = timeAndOffsetToLocalIso8601(
          dateTime,
          dateOffset * offsetMultiplier
        );

        keyTags = {
          utc: extendedDateObject(utcExt),
          local: extendedDateObject(localExt),
        };
      } else {
        keyTags = extendedDateObject(utcExt.slice(0, -1));
      }

      tags = {
        ...tags,
        [key]: keyTags,
      };
    }
  });

  return tags;
}

export function getMetadataExtendedFilenameDate({
  exiftoolMetadata,
  filename,
  pattern,
  replace,
  tzParse,
  tzLocal,
}: {
  exiftoolMetadata: Tags;
  filename: string;
  pattern?: string;
  replace?: string;
  tzParse?: string;
  tzLocal?: string;
}): Record<string, unknown> | undefined {
  logger.debug(
    `metadata.getMetadataExtendedFilenameDate({exiftoolMetadata: "OMITTED FOR LOGGING", filename: "${filename}", pattern: "${pattern}", replace: "${replace}", tzParse: "${tzParse}", tzLocal: "${tzLocal}"})`
  );

  if (!pattern || !replace) return;

  if ((tzParse && !tzLocal) || (!tzParse && tzLocal))
    throw `Unable to convert timezone without tzParse and tzLocal`;

  const exifMetadata = { exiftool: exiftoolMetadata };

  let filenameDate = {};

  if (tzParse && tzLocal) {
    const tzParseValue = metadata.replaceMetadataReferences({
      value: tzParse,
      metadata: exifMetadata,
    });
    const tzLocalValue = metadata.replaceMetadataReferences({
      value: tzLocal,
      metadata: exifMetadata,
    });
    const filenameExifDateTime = ExifDateTime.fromEXIF(
      filename.replace(new RegExp(pattern), replace),
      tzParseValue
    );
    const utcExt = new Date(String(filenameExifDateTime)).toISOString();
    const localExt = DateTime.fromISO(utcExt)
      .setZone(tzLocalValue as string)
      .toISO({ includeOffset: false });
    filenameDate = {
      utc: extendedDateObject(utcExt),
      local: extendedDateObject(localExt),
    };
  } else {
    const filenameExifDateTime = filename.replace(new RegExp(pattern), replace);
    const ext = DateTime.fromISO(filenameExifDateTime).toISO({
      includeOffset: false,
    });
    filenameDate = extendedDateObject(ext);
  }

  return { FilenameDate: filenameDate };
}

function extendedDateObject(extended: string) {
  const basic = iso8601ExtendedToBasic(extended);
  return {
    ISO8601: { extended, basic },
    year: {
      numeric: parseInt(basic.substr(0, 4)).toString(),
      "2-digit": basic.substr(2, 2),
    },
    month: {
      numeric: parseInt(basic.substr(4, 2)).toString(),
      "2-digit": basic.substr(4, 2),
    },
    day: {
      numeric: parseInt(basic.substr(6, 2)).toString(),
      "2-digit": basic.substr(6, 2),
    },
    hour: {
      numeric: parseInt(basic.substr(9, 2)).toString(),
      "2-digit": basic.substr(9, 2),
    },
    minute: {
      numeric: parseInt(basic.substr(11, 2)).toString(),
      "2-digit": basic.substr(11, 2),
    },
    second: {
      numeric: parseInt(basic.substr(13, 2)).toString(),
      "2-digit": basic.substr(13, 2),
    },
    millisecond: {
      numeric: parseInt(basic.substr(15, 3)).toString(),
      "3-digit": basic.substr(15, 3),
    },
  };
}

function iso8601ExtendedToBasic(extended: string): string {
  return extended.replace(/-|:|\./g, "");
}

function timeAndOffsetToLocalIso8601(time: number, offset: number): string {
  return new Date(time + offset).toISOString().slice(0, -1);
}

export function replaceMetadataReferences({
  value,
  metadata,
}: {
  value: string;
  metadata: Record<string, unknown>;
}): string {
  logger.debug(
    `metadata.replaceExifReferences({value: "${value}", metadata: "${metadata}"})`
  );
  const matches = value.match(/{.+?}/g) || [];
  for (const match of matches) {
    const matchKeys = match.replace(/({|})/g, "").split(".");
    let tagObject = metadata;
    let tagValue = "";
    for (const matchKey of matchKeys) {
      tagObject = <Record<string, unknown>>tagObject[matchKey];
      if (tagObject === undefined)
        throw `Unable to extract ${matchKeys.join(".")} from EXIF data`;
    }
    tagValue = tagValue || String(tagObject);
    value = value.replace(match, tagValue);
  }
  return value;
}

export function replaceFileExtension({
  filename,
  extensionCase,
  extensionPattern,
}: {
  filename: string;
  extensionCase: string | undefined;
  extensionPattern: string;
}): string {
  logger.debug(
    `metadata.replaceFileExtension({filename: "${filename}", extensionCase: "${extensionCase}", extensionPattern: "${extensionPattern}"})`
  );

  if (!extensionCase) return filename;

  const srcExtensionMatch = filename.match(extensionPattern);
  if (srcExtensionMatch == null) {
    throw `Unable to find file extension matching ${extensionPattern} in ${filename}`;
  }
  const srcExtension = srcExtensionMatch[1];
  const extensionRegex = new RegExp(`${srcExtension}$`);

  if (extensionCase === "upper") {
    return filename.replace(extensionRegex, srcExtension.toUpperCase());
  }
  if (extensionCase === "lower") {
    return filename.replace(extensionRegex, srcExtension.toLowerCase());
  }
  return filename;
}
