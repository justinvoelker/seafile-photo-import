import { ExifDateTime, exiftool } from "exiftool-vendored";
import { logger } from "./logger";
import {
  getMetadataExifTool,
  getMetadataExtendedDates,
  getMetadataExtendedFilenameDate,
  replaceMetadataReferences,
  replaceFileExtension,
} from "./metadata";

import * as file from "./file";
import * as metadata from "./metadata";

jest.mock("./config", () => ({
  config: jest.fn(() => {
    return {
      logLevel: "warn",
      seafile: { hostname: "host", username: "user", password: "pass" },
    };
  }),
}));

afterAll(() => {
  exiftool.end();
});

describe("getMetadataExifTool", () => {
  it("should return exiftool metadata", async () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const removeCallbackMock = jest.fn();
    const downloadTmpFileSpy = jest
      .spyOn(file, "downloadTmpFile")
      .mockResolvedValue({
        name: "/path/name",
        fd: 0,
        removeCallback: removeCallbackMock,
      });
    const exiftoolReadSpy = jest.spyOn(exiftool, "read").mockResolvedValue({
      Directory: "/tmp",
      FileName: "tmp-1234",
      DateTimeOriginal: new ExifDateTime(
        2021,
        6,
        7,
        2,
        4,
        6,
        0,
        undefined,
        "2021:06:07 02:04:06.000"
      ),
      CreateDate: new ExifDateTime(
        2021,
        6,
        11,
        18,
        7,
        50,
        0,
        +120,
        "2021:06:11 18:07:50.000+02:00",
        "UTC+2"
      ),
      SubSecCreateDate: new ExifDateTime(
        2021,
        6,
        11,
        22,
        7,
        50,
        209,
        -240,
        "2021:06:11 22:07:50.209-04:00",
        "UTC-4"
      ),
    });

    const metadataExifTool = await getMetadataExifTool({
      libraryId: "00000000",
      path: "path/",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(downloadTmpFileSpy).toHaveBeenCalledTimes(1);
    expect(downloadTmpFileSpy).toHaveBeenCalledWith({
      libraryId: "00000000",
      path: "path/",
    });
    expect(exiftoolReadSpy).toHaveBeenCalledTimes(1);
    expect(exiftoolReadSpy).toHaveBeenCalledWith("/path/name");
    expect(removeCallbackMock).toHaveBeenCalledTimes(1);

    expect(metadataExifTool.Directory).toBe("/tmp");
    expect(metadataExifTool.FileName).toBe("tmp-1234");
    expect(metadataExifTool.CreateDate).toBeInstanceOf(ExifDateTime);
    expect(metadataExifTool.SubSecCreateDate).toBeInstanceOf(ExifDateTime);
  });
});

describe("getMetadataExtendedDates", () => {
  it("should return extended dates metadata", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const exifToolTags = {
      DateTimeOriginal: new ExifDateTime(
        2021,
        6,
        7,
        2,
        4,
        6,
        0,
        undefined,
        "2021:06:07 02:04:06.000"
      ),
      CreateDate: new ExifDateTime(
        2021,
        6,
        11,
        18,
        7,
        50,
        0,
        +120,
        "2021:06:11 18:07:50.000+02:00",
        "UTC+2"
      ),
      SubSecCreateDate: new ExifDateTime(
        2021,
        6,
        11,
        22,
        7,
        50,
        209,
        -240,
        "2021:06:11 22:07:50.209-04:00",
        "UTC-4"
      ),
    };

    const metadataExtendedDates = getMetadataExtendedDates({
      exifToolTags,
      metadataModifiers: [],
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);

    expect(metadataExtendedDates).toStrictEqual({
      DateTimeOriginal: {
        ISO8601: {
          extended: "2021-06-07T02:04:06.000",
          basic: "20210607T020406000",
        },
        year: { numeric: "2021", "2-digit": "21" },
        month: { numeric: "6", "2-digit": "06" },
        day: { numeric: "7", "2-digit": "07" },
        hour: { numeric: "2", "2-digit": "02" },
        minute: { numeric: "4", "2-digit": "04" },
        second: { numeric: "6", "2-digit": "06" },
        millisecond: { numeric: "0", "3-digit": "000" },
      },
      CreateDate: {
        local: {
          ISO8601: {
            extended: "2021-06-11T18:07:50.000",
            basic: "20210611T180750000",
          },
          year: { numeric: "2021", "2-digit": "21" },
          month: { numeric: "6", "2-digit": "06" },
          day: { numeric: "11", "2-digit": "11" },
          hour: { numeric: "18", "2-digit": "18" },
          minute: { numeric: "7", "2-digit": "07" },
          second: { numeric: "50", "2-digit": "50" },
          millisecond: { numeric: "0", "3-digit": "000" },
        },
        utc: {
          ISO8601: {
            extended: "2021-06-11T16:07:50.000Z",
            basic: "20210611T160750000Z",
          },
          year: { numeric: "2021", "2-digit": "21" },
          month: { numeric: "6", "2-digit": "06" },
          day: { numeric: "11", "2-digit": "11" },
          hour: { numeric: "16", "2-digit": "16" },
          minute: { numeric: "7", "2-digit": "07" },
          second: { numeric: "50", "2-digit": "50" },
          millisecond: { numeric: "0", "3-digit": "000" },
        },
      },
      SubSecCreateDate: {
        local: {
          ISO8601: {
            extended: "2021-06-11T22:07:50.209",
            basic: "20210611T220750209",
          },
          year: { numeric: "2021", "2-digit": "21" },
          month: { numeric: "6", "2-digit": "06" },
          day: { numeric: "11", "2-digit": "11" },
          hour: { numeric: "22", "2-digit": "22" },
          minute: { numeric: "7", "2-digit": "07" },
          second: { numeric: "50", "2-digit": "50" },
          millisecond: { numeric: "209", "3-digit": "209" },
        },
        utc: {
          ISO8601: {
            extended: "2021-06-12T02:07:50.209Z",
            basic: "20210612T020750209Z",
          },
          year: { numeric: "2021", "2-digit": "21" },
          month: { numeric: "6", "2-digit": "06" },
          day: { numeric: "12", "2-digit": "12" },
          hour: { numeric: "2", "2-digit": "02" },
          minute: { numeric: "7", "2-digit": "07" },
          second: { numeric: "50", "2-digit": "50" },
          millisecond: { numeric: "209", "3-digit": "209" },
        },
      },
    });
  });

  it("should return extended dates metadata with DATE_RAW_VALUE_UTC_AS_LOCAL modifier", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const exifToolTags = {
      SubSecCreateDate: new ExifDateTime(
        2021,
        6,
        11,
        22,
        7,
        50,
        209,
        -240,
        "2021:06:11 22:07:50.209",
        "UTC-4"
      ),
    };

    const metadataExtendedDates = getMetadataExtendedDates({
      exifToolTags,
      metadataModifiers: ["DATE_RAW_VALUE_UTC_AS_LOCAL"],
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);

    expect(metadataExtendedDates).toStrictEqual({
      SubSecCreateDate: {
        local: {
          ISO8601: {
            extended: "2021-06-11T18:07:50.209",
            basic: "20210611T180750209",
          },
          year: { numeric: "2021", "2-digit": "21" },
          month: { numeric: "6", "2-digit": "06" },
          day: { numeric: "11", "2-digit": "11" },
          hour: { numeric: "18", "2-digit": "18" },
          minute: { numeric: "7", "2-digit": "07" },
          second: { numeric: "50", "2-digit": "50" },
          millisecond: { numeric: "209", "3-digit": "209" },
        },
        utc: {
          ISO8601: {
            extended: "2021-06-12T02:07:50.209Z",
            basic: "20210612T020750209Z",
          },
          year: { numeric: "2021", "2-digit": "21" },
          month: { numeric: "6", "2-digit": "06" },
          day: { numeric: "12", "2-digit": "12" },
          hour: { numeric: "2", "2-digit": "02" },
          minute: { numeric: "7", "2-digit": "07" },
          second: { numeric: "50", "2-digit": "50" },
          millisecond: { numeric: "209", "3-digit": "209" },
        },
      },
    });
  });
});

describe("getMetadataExtendedFilenameDate", () => {
  it("should return undefined when pattern and replace are not provided", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const metadataExtendedFilenameDate = getMetadataExtendedFilenameDate({
      exiftoolMetadata: { tz: "America/Detroit" },
      filename: "PXL_20210723_000305155.mp4",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(metadataExtendedFilenameDate).toBeUndefined();
  });

  it("should return undefined when pattern is provided and replace is not provided", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const metadataExtendedFilenameDate = getMetadataExtendedFilenameDate({
      exiftoolMetadata: { tz: "America/Detroit" },
      filename: "PXL_20210723_000305155.mp4",
      pattern: "pattern",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(metadataExtendedFilenameDate).toBeUndefined();
  });

  it("should return undefined when pattern is not provided and replace is provided", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const metadataExtendedFilenameDate = getMetadataExtendedFilenameDate({
      exiftoolMetadata: { tz: "America/Detroit" },
      filename: "PXL_20210723_000305155.mp4",
      replace: "replace",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(metadataExtendedFilenameDate).toBeUndefined();
  });

  it("should throw an error when tzParse is provided and tzLocal is not provided", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    expect.assertions(2);
    try {
      getMetadataExtendedFilenameDate({
        exiftoolMetadata: { tz: "America/Detroit" },
        filename: "PXL_20210723_000305155.mp4",
        pattern: "pattern",
        replace: "replace",
        tzParse: "tzParse",
      });
    } catch (err) {
      expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
      expect(err).toContain("Unable to convert timezone");
    }
  });

  it("should throw an error when tzParse is not provided and tzLocal is provided", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    expect.assertions(2);
    try {
      getMetadataExtendedFilenameDate({
        exiftoolMetadata: { tz: "America/Detroit" },
        filename: "PXL_20210723_000305155.mp4",
        pattern: "pattern",
        replace: "replace",
        tzLocal: "tzLocal",
      });
    } catch (err) {
      expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
      expect(err).toContain("Unable to convert timezone");
    }
  });

  it("should return filename datetime metadata without timezone parsing", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const metadataExtendedFilenameDate = getMetadataExtendedFilenameDate({
      exiftoolMetadata: { tz: "America/Detroit" },
      filename: "PXL_20210723_000305155.mp4",
      pattern:
        "^PXL_(\\d{4})(\\d{2})(\\d{2})_(\\d{2})(\\d{2})(\\d{2})(\\d{3}).*$",
      replace: "$1-$2-$3T$4:$5:$6.$7",
      // tzParse,
      // tzLocal,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(metadataExtendedFilenameDate).toStrictEqual({
      FilenameDate: {
        ISO8601: {
          extended: "2021-07-23T00:03:05.155",
          basic: "20210723T000305155",
        },
        year: { numeric: "2021", "2-digit": "21" },
        month: { numeric: "7", "2-digit": "07" },
        day: { numeric: "23", "2-digit": "23" },
        hour: { numeric: "0", "2-digit": "00" },
        minute: { numeric: "3", "2-digit": "03" },
        second: { numeric: "5", "2-digit": "05" },
        millisecond: { numeric: "155", "3-digit": "155" },
      },
    });
  });

  it("should return filename datetime metadata with timezone parsed as UTC", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const metadataReplaceMetadataReferencesSpy = jest
      .spyOn(metadata, "replaceMetadataReferences")
      .mockReturnValueOnce("UTC")
      .mockReturnValueOnce("America/Detroit");

    const metadataExtendedFilenameDate = getMetadataExtendedFilenameDate({
      exiftoolMetadata: {},
      filename: "PXL_20210723_000305155.mp4",
      pattern:
        "^PXL_(\\d{4})(\\d{2})(\\d{2})_(\\d{2})(\\d{2})(\\d{2})(\\d{3}).*$",
      replace: "$1-$2-$3T$4:$5:$6.$7",
      tzParse: "mocked",
      tzLocal: "mocked",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(metadataReplaceMetadataReferencesSpy).toHaveBeenCalledTimes(2);
    expect(metadataExtendedFilenameDate).toStrictEqual({
      FilenameDate: {
        utc: {
          ISO8601: {
            extended: "2021-07-23T00:03:05.155Z",
            basic: "20210723T000305155Z",
          },
          year: { numeric: "2021", "2-digit": "21" },
          month: { numeric: "7", "2-digit": "07" },
          day: { numeric: "23", "2-digit": "23" },
          hour: { numeric: "0", "2-digit": "00" },
          minute: { numeric: "3", "2-digit": "03" },
          second: { numeric: "5", "2-digit": "05" },
          millisecond: { numeric: "155", "3-digit": "155" },
        },
        local: {
          ISO8601: {
            extended: "2021-07-22T20:03:05.155",
            basic: "20210722T200305155",
          },
          year: { numeric: "2021", "2-digit": "21" },
          month: { numeric: "7", "2-digit": "07" },
          day: { numeric: "22", "2-digit": "22" },
          hour: { numeric: "20", "2-digit": "20" },
          minute: { numeric: "3", "2-digit": "03" },
          second: { numeric: "5", "2-digit": "05" },
          millisecond: { numeric: "155", "3-digit": "155" },
        },
      },
    });
  });

  it("should return filename datetime metadata with timezone parsed as non-UTC", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();
    const metadataReplaceMetadataReferencesSpy = jest
      .spyOn(metadata, "replaceMetadataReferences")
      .mockReturnValueOnce("UTC+2")
      .mockReturnValueOnce("America/Detroit");

    const metadataExtendedFilenameDate = getMetadataExtendedFilenameDate({
      exiftoolMetadata: {},
      filename: "PXL_20210723_040305155.mp4",
      pattern:
        "^PXL_(\\d{4})(\\d{2})(\\d{2})_(\\d{2})(\\d{2})(\\d{2})(\\d{3}).*$",
      replace: "$1-$2-$3T$4:$5:$6.$7",
      tzParse: "mocked",
      tzLocal: "mocked",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(metadataReplaceMetadataReferencesSpy).toHaveBeenCalledTimes(2);
    expect(metadataExtendedFilenameDate).toStrictEqual({
      FilenameDate: {
        utc: {
          ISO8601: {
            extended: "2021-07-23T02:03:05.155Z",
            basic: "20210723T020305155Z",
          },
          year: { numeric: "2021", "2-digit": "21" },
          month: { numeric: "7", "2-digit": "07" },
          day: { numeric: "23", "2-digit": "23" },
          hour: { numeric: "2", "2-digit": "02" },
          minute: { numeric: "3", "2-digit": "03" },
          second: { numeric: "5", "2-digit": "05" },
          millisecond: { numeric: "155", "3-digit": "155" },
        },
        local: {
          ISO8601: {
            extended: "2021-07-22T22:03:05.155",
            basic: "20210722T220305155",
          },
          year: { numeric: "2021", "2-digit": "21" },
          month: { numeric: "7", "2-digit": "07" },
          day: { numeric: "22", "2-digit": "22" },
          hour: { numeric: "22", "2-digit": "22" },
          minute: { numeric: "3", "2-digit": "03" },
          second: { numeric: "5", "2-digit": "05" },
          millisecond: { numeric: "155", "3-digit": "155" },
        },
      },
    });
  });
});

describe("replaceMetadataReferences", () => {
  const exifToolTags = {
    FileTypeExtension: "jpg",
    DateTimeOriginal: new ExifDateTime(
      2021,
      5,
      29,
      14,
      13,
      41,
      472,
      undefined,
      "2021:05:29 14:13:41.472"
    ),
    CreateDate: new ExifDateTime(
      2021,
      5,
      29,
      14,
      13,
      41,
      472,
      -300,
      "2021:05:29 14:13:41.472-05:00",
      "UTC-5"
    ),
  };
  const metadata = {
    exiftool: exifToolTags,
    extended: getMetadataExtendedDates({ exifToolTags, metadataModifiers: [] }),
  };

  it("should return original string with no replacements", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const dstDirectory = replaceMetadataReferences({
      value: "StringWithoutReferences",
      metadata,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(dstDirectory).toBe("StringWithoutReferences");
  });

  it("should return string with exiftool replaced values", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const dstDirectory = replaceMetadataReferences({
      value:
        "/{exiftool.CreateDate.year}/{exiftool.CreateDate.month}/{exiftool.CreateDate.day}/{exiftool.FileTypeExtension}/",
      metadata,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(dstDirectory).toBe("/2021/5/29/jpg/");
  });

  it("should return string with extended replaced values without timezone", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const dstDirectory = replaceMetadataReferences({
      value:
        "/{extended.DateTimeOriginal.year.2-digit}/{extended.DateTimeOriginal.month.2-digit}/{extended.DateTimeOriginal.day.2-digit}/{extended.DateTimeOriginal.ISO8601.basic}/",
      metadata,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(dstDirectory).toBe("/21/05/29/20210529T141341472/");
  });

  it("should return string with extended replaced values with timezone", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const dstDirectory = replaceMetadataReferences({
      value:
        "/{extended.CreateDate.utc.year.2-digit}/{extended.CreateDate.utc.month.2-digit}/{extended.CreateDate.utc.day.2-digit}/{extended.CreateDate.utc.ISO8601.basic}/",
      metadata,
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(dstDirectory).toBe("/21/05/29/20210529T191341472Z/");
  });

  it("should throw an error for non-existent EXIF data", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    expect.assertions(2);
    try {
      replaceMetadataReferences({ value: "{NonExistantReference}", metadata });
    } catch (err) {
      expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
      expect(err).toContain("NonExistantReference");
    }
  });
});

describe("replaceFileExtension", () => {
  it("should return original filename without changing extension casing for undefined case", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const filename = replaceFileExtension({
      filename: "IMG_0001.Jpg",
      extensionCase: undefined,
      extensionPattern: ".*\\.(.*)",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(filename).toBe("IMG_0001.Jpg");
  });

  it("should return original filename without changing extension casing for invalid case", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const filename = replaceFileExtension({
      filename: "IMG_0001.Jpg",
      extensionCase: "invalid",
      extensionPattern: ".*\\.(.*)",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(filename).toBe("IMG_0001.Jpg");
  });

  it("should return string with lower cased extension", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const filename = replaceFileExtension({
      filename: "IMG_0001.Jpg",
      extensionCase: "lower",
      extensionPattern: ".*\\.(.*)",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(filename).toBe("IMG_0001.jpg");
  });

  it("should return string with upper cased extension", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const filename = replaceFileExtension({
      filename: "IMG_0001.Jpg",
      extensionCase: "upper",
      extensionPattern: ".*\\.(.*)",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(filename).toBe("IMG_0001.JPG");
  });

  it("should return string with upper cased extension for custom pattern that exists", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const filename = replaceFileExtension({
      filename: "IMG_0001.Mp.Jpg",
      extensionCase: "upper",
      extensionPattern: ".*?\\.((Mp\\.)?Jpg)$",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(filename).toBe("IMG_0001.MP.JPG");
  });

  it("should return string with upper cased extension for custom pattern that does not exist", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    const filename = replaceFileExtension({
      filename: "IMG_0001.Jpg",
      extensionCase: "upper",
      extensionPattern: ".*?\\.((Mp\\.)?Jpg)$",
    });

    expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
    expect(filename).toBe("IMG_0001.JPG");
  });

  it("should throw an error for an extension pattern that cannot be found", () => {
    const loggerDebugSpy = jest.spyOn(logger, "debug").mockImplementation();

    expect.assertions(2);
    try {
      replaceFileExtension({
        filename: "IMG_0001.Jpg",
        extensionCase: "lower",
        extensionPattern: ".*\\.(mov)",
      });
    } catch (err) {
      expect(loggerDebugSpy).toHaveBeenCalledTimes(1);
      expect(err).toContain("Unable to find file extension");
    }
  });
});
