import fs from "fs";
import tmp, { FileResult } from "tmp";
import axios from "axios";
import { logger } from "./logger";
import { getFileLink } from "./seafile";

export async function downloadTmpFile({
  libraryId,
  path,
}: {
  libraryId: string;
  path: string;
}): Promise<FileResult> {
  logger.debug(
    `file.downloadTmpFile({libraryId: "${libraryId}", path: "${path}"})`
  );
  const tmpFile = tmp.fileSync();
  const writer = fs.createWriteStream(tmpFile.name);

  const link = await getFileLink({ libraryId, path });
  const { data: stream } = await axios.get(link, { responseType: "stream" });
  await stream.pipe(writer);
  await new Promise((resolve, reject) => {
    writer.on("finish", resolve);
    writer.on("error", reject);
  });

  return tmpFile;
}

export function deleteTmpFile({ file }: { file: FileResult }): void {
  logger.debug(`file.deleteTmpFile({file: "${file.name}"})`);
  file.removeCallback();
}
