import axiosVendor from "axios";
import { logger, logEdits } from "./logger";
import { axios as axiosSeafile } from "./axios";

export async function createDirectory({
  dryRun,
  libraryId,
  libraryName,
  path,
}: {
  dryRun: boolean;
  libraryId: string;
  libraryName: string;
  path: string;
}): Promise<void> {
  logger.debug(
    `seafile.createDirectory({dryRun: ${dryRun}, libraryId: "${libraryId}", libraryName: "${libraryName}", path: "${path}"})`
  );
  logger.verbose(`Create directory "/${libraryName}${path}"`, {
    dryRun,
  });
  if (dryRun) return;
  await axiosSeafile.post(
    `/api2/repos/${libraryId}/dir/?p=${path}`,
    `operation=mkdir`
  );
}

export async function directoryExists({
  libraryId,
  path,
}: {
  libraryId: string;
  path: string;
}): Promise<boolean> {
  logger.debug(
    `seafile.directoryExists({libraryId: "${libraryId}", path: "${path}"})`
  );
  try {
    await axiosSeafile.get(
      `/api/v2.1/repos/${libraryId}/dir/detail/?path=${path}`
    );
    return true;
  } catch (error) {
    if (error.response.status == "404") return false;
    throw error;
  }
}

export async function fileExists({
  libraryId,
  path,
}: {
  libraryId: string;
  path: string;
}): Promise<boolean> {
  logger.debug(
    `seafile.fileExists({libraryId: "${libraryId}", path: "${path}"})`
  );
  try {
    await axiosSeafile.get(`/api2/repos/${libraryId}/file/detail/?p=${path}`);
    return true;
  } catch (error) {
    if (error.response.status == "404") return false;
    throw error;
  }
}

export async function getAuthToken({
  hostname,
  username,
  password,
}: {
  hostname: string;
  username: string;
  password: string;
}): Promise<string> {
  logger.debug(
    `seafile.getAuthToken({hostname: "${hostname}", username: "${username}", password: "HIDDEN"})`
  );
  const { data } = await axiosVendor.post(`${hostname}/api2/auth-token/`, {
    username,
    password,
  });
  return data.token;
}

export async function getFileLink({
  libraryId,
  path,
}: {
  libraryId: string;
  path: string;
}): Promise<string> {
  logger.debug(
    `seafile.getFileLink({libraryId: "${libraryId}", path: "${path}"})`
  );
  const { data } = await axiosSeafile.get(
    `/api2/repos/${libraryId}/file?p=${path}`
  );
  return data;
}

export async function getFiles({
  libraryId,
  libraryName,
  path,
  recursive = false,
}: {
  libraryId: string;
  libraryName: string;
  path: string;
  recursive?: boolean;
}): Promise<Array<Record<string, unknown>>> {
  logger.debug(
    `seafile.getFiles({libraryId: "${libraryId}", libraryName: "${libraryName}", path: "${path}", recursive: ${recursive}})`
  );
  logger.verbose(`Read "/${libraryName}/${path}" directory`);
  const { data } = await axiosSeafile.get(
    `/api2/repos/${libraryId}/dir?p=${path}&t=f&recursive=${
      recursive ? "1" : "0"
    }`
  );
  return data;
}

export async function getLibraryId({
  name,
}: {
  name: string;
}): Promise<string> {
  logger.debug(`seafile.getLibraryId({name: "${name}"})`);
  const {
    data: libraries,
  }: {
    data: Array<Record<string, unknown>>;
  } = await axiosSeafile.get("/api2/repos?type=mine");
  const library = libraries.find((library) => library.name == name);
  if (!library) throw `Library "${name}" does not exist`;

  return library.id as string;
}

export async function moveFile({
  dryRun,
  srcLibraryId,
  srcLibraryName,
  srcFilePath,
  dstLibraryId,
  dstLibraryName,
  dstFilePath,
}: {
  dryRun: boolean;
  srcLibraryId: string;
  srcLibraryName: string;
  srcFilePath: string;
  dstLibraryId: string;
  dstLibraryName: string;
  dstFilePath: string;
}): Promise<void> {
  logger.debug(
    `seafile.moveFile({dryRun: ${dryRun}, srcLibraryId: "${srcLibraryId}", srcLibraryName: "${srcLibraryName}", srcFilePath: "${srcFilePath}", dstLibraryId: "${dstLibraryId}", dstLibraryName: "${dstLibraryName}", dstFilePath: "${dstFilePath}"})`
  );
  const srcFileDir = `/${srcFilePath.split("/").slice(1, -1).join("/")}`;
  const dstFileDir = `/${dstFilePath.split("/").slice(1, -1).join("/")}`;
  if (srcFileDir == dstFileDir) return;
  if (!dryRun) {
    await axiosSeafile.post(
      `/api/v2.1/repos/${srcLibraryId}/file/?p=${srcFilePath}`,
      `operation=move&dst_repo=${dstLibraryId}&dst_dir=${dstFileDir}`
    );
  }
  logger.info(
    `Move "/${srcLibraryName}${srcFilePath}" to "/${dstLibraryName}${dstFilePath}"`,
    { dryRun }
  );
  logEdits(
    "Move",
    `/${srcLibraryName}${srcFilePath}`,
    `/${dstLibraryName}${dstFilePath}`,
    dryRun
  );
}

export async function renameAndMoveFile({
  dryRun,
  srcLibraryId,
  srcLibraryName,
  srcFilePath,
  dstLibraryId,
  dstLibraryName,
  dstFilePath,
}: {
  dryRun: boolean;
  srcLibraryId: string;
  srcLibraryName: string;
  srcFilePath: string;
  dstLibraryId: string;
  dstLibraryName: string;
  dstFilePath: string;
}): Promise<void> {
  logger.debug(
    `seafile.renameAndMoveFile({dryRun: ${dryRun}, srcLibraryId: "${srcLibraryId}", srcLibraryName: "${srcLibraryName}", srcFilePath: "${srcFilePath}", dstLibraryId: "${dstLibraryId}", dstLibraryName: "${dstLibraryName}", dstFilePath: "${dstFilePath}"})`
  );

  let renamed = false;
  let moved = false;

  const srcFileDir = `/${srcFilePath.split("/").slice(1, -1).join("/")}`;
  const srcFileName = srcFilePath.split("/")[srcFilePath.split("/").length - 1];
  const dstFileDir = `/${dstFilePath.split("/").slice(1, -1).join("/")}`;
  const dstFileName = dstFilePath.split("/")[dstFilePath.split("/").length - 1];

  if (dstFileName != srcFileName) {
    if (!dryRun) {
      await axiosSeafile.post(
        `/api/v2.1/repos/${srcLibraryId}/file/?p=${srcFilePath}`,
        `operation=rename&newname=${dstFileName}`
      );
    }
    renamed = true;
  }

  if (dstFileDir != srcFileDir || srcLibraryId != dstLibraryId) {
    if (!dryRun) {
      const moveSrcFileName = renamed
        ? `${srcFileDir}/${dstFileName}`
        : srcFilePath;
      await axiosSeafile.post(
        `/api/v2.1/repos/${srcLibraryId}/file/?p=${moveSrcFileName}`,
        `operation=move&dst_repo=${dstLibraryId}&dst_dir=${dstFileDir}`
      );
    }
    moved = true;
  }

  if (renamed || moved) {
    logger.info(
      `RenameAndMove "/${srcLibraryName}${srcFilePath}" to "/${dstLibraryName}${dstFilePath}"`,
      { dryRun }
    );
    logEdits(
      "RenameAndMove",
      `/${srcLibraryName}${srcFilePath}`,
      `/${dstLibraryName}${dstFilePath}`,
      dryRun
    );
  }
}

export async function renameFile({
  dryRun,
  libraryId,
  libraryName,
  path,
  name,
}: {
  dryRun: boolean;
  libraryId: string;
  libraryName: string;
  path: string;
  name: string;
}): Promise<string> {
  logger.debug(
    `seafile.renameFile({dryRun: ${dryRun}, libraryId: "${libraryId}", libraryName: "${libraryName}", path: "${path}", name: "${name}"})`
  );
  const srcName = path.split("/")[path.split("/").length - 1];
  const srcDirectory = path.split("/").slice(0, -1).join("/");
  if (name == srcName) return name;
  if (!dryRun) {
    await axiosSeafile.post(
      `/api/v2.1/repos/${libraryId}/file/?p=${path}`,
      `operation=rename&newname=${name}`
    );
  }
  logger.info(
    `Rename "/${libraryName}${path}" to "/${libraryName}${srcDirectory}/${name}"`,
    { dryRun }
  );
  logEdits(
    "Rename",
    `/${libraryName}${path}`,
    `/${libraryName}${srcDirectory}/${name}`,
    dryRun
  );

  return name;
}
