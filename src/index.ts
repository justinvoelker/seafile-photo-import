import { config as getConfig } from "./helpers/config";
import { logger } from "./helpers/logger";
import { createDirectoryHierarchy } from "./helpers/directory";
import {
  getMetadataExtendedFilenameDate,
  getMetadataExifTool,
  getMetadataExtendedDates,
  replaceFileExtension,
  replaceMetadataReferences,
} from "./helpers/metadata";
import {
  fileExists,
  getFiles,
  getLibraryId,
  renameAndMoveFile,
} from "./helpers/seafile";
import {
  ensureSidecarFileExists as ensureRequiredSidecarFileExists,
  getSidecarFilename,
} from "./helpers/sidecar";
import { getListHash } from "./utils/crypto";
import { matchesPattern } from "./utils/regex";
import { stripExtraSlashes } from "./utils/string";

const config = getConfig();
loop();
setInterval(loop, config.intervalSeconds * 1000);

let libraryHashes: Record<string, string> = {};

async function loop() {
  try {
    // Loop through each task
    for (const task of config.tasks) {
      // Set logger log level
      logger.level = task.logLevel || config.logLevel;
      logger.verbose(`Task "${task.name}" log level set to "${logger.level}"`);

      // Skip everything if task is disabled
      if (!task.enabled) {
        logger.verbose(`Task "${task.name}" is disabled`);
        continue;
      }

      // Log start of task
      logger.verbose(`Task "${task.name}" started`);

      // Get library and directory names
      const [srcLibraryName, ...srcDirectories] = stripExtraSlashes(
        task.srcDirectory
      ).split("/");
      const [dstLibraryName, ...dstDirectories] = stripExtraSlashes(
        task.dstDirectory
      ).split("/");

      // Get library IDs
      const srcLibraryId = await getLibraryId({ name: srcLibraryName });
      const dstLibraryId = await getLibraryId({ name: dstLibraryName });

      // Get source files
      const srcFiles = await getFiles({
        libraryId: srcLibraryId,
        libraryName: srcLibraryName,
        path: srcDirectories.join("/"),
        recursive: true,
      });

      // Library/task hash key to ensure this specific library and task don't reprocess this list of files
      const listHashKey = `${srcLibraryName}_${task.name}`;

      // Only need to process files if the library files have changed
      if (getListHash(srcFiles) == libraryHashes[listHashKey]) {
        logger.verbose(`Task "${task.name}" is finishing (no file changes)`);
        continue;
      }

      // Loop through each source file
      for (const srcFile of srcFiles) {
        // Each file will pass/fail on its own
        try {
          // Check if source file matches source pattern
          if (!matchesPattern(srcFile.name, task.srcFilenamePattern)) {
            logger.verbose(
              `File "${srcFile.name}" does not match pattern ${task.srcFilenamePattern}`
            );
            continue;
          }

          // Continue processing source file
          logger.verbose(`File "${srcFile.name}" started`);

          // Get metadata for source file
          const metadataExiftool = await getMetadataExifTool({
            libraryId: srcLibraryId,
            path: `/${stripExtraSlashes(srcFile.parent_dir as string)}/${
              srcFile.name
            }`,
          });
          const metadataExtendedDates = getMetadataExtendedDates({
            exifToolTags: metadataExiftool,
            metadataModifiers: task.metadataModifiers,
          });
          const metadataExtendedFilenameDate = getMetadataExtendedFilenameDate({
            exiftoolMetadata: metadataExiftool,
            filename: srcFile.name as string,
            pattern: task.srcFilenameDateTimePattern,
            replace: task.srcFilenameDateTimeReplace,
            tzParse: task.srcFilenameDateTimeTimezoneParse,
            tzLocal: task.srcFilenameDateTimeTimezoneLocal,
          });
          const metadata = {
            exiftool: metadataExiftool,
            extended: {
              ...metadataExtendedDates,
              ...metadataExtendedFilenameDate,
            },
          };
          logger.debug(
            `File "/${stripExtraSlashes(srcFile.parent_dir as string)}/${
              srcFile.name
            }" metadata: ${JSON.stringify(metadata)}`
          );

          // Replace meetadata within destination directory
          const dstDirectory = replaceMetadataReferences({
            value: `/${dstDirectories.join("/")}`,
            metadata,
          });

          // Create destination directory hierarchy
          await createDirectoryHierarchy({
            libraryId: dstLibraryId,
            libraryName: dstLibraryName,
            directory: dstDirectory,
            dryRun: task.dryRun,
          });

          // Begin destination filename renaming
          let dstFileName = srcFile.name as string;

          // Change case of file extension
          dstFileName = replaceFileExtension({
            filename: dstFileName,
            extensionCase: task.dstFilenameExtensionCase,
            extensionPattern: task.dstFilenameExtensionCasePattern,
          });

          // Loop through each step
          for (const step of task.steps) {
            logger.verbose(`Step "${step.name}" old value "${dstFileName}"`);

            const stepFind = new RegExp(step.find, step.flag);
            const stepRepl = replaceMetadataReferences({
              value: step.repl,
              metadata,
            });

            dstFileName = dstFileName.replace(stepFind, stepRepl);
            logger.verbose(`Step "${step.name}" new value "${dstFileName}"`);
          }

          // Ensure required sidecar files exist
          for (const sidecar of task.sidecars) {
            await ensureRequiredSidecarFileExists({
              libraryId: srcLibraryId,
              primaryFileName: srcFile.name as string,
              primaryFilePath: srcFile.parent_dir as string,
              findPrimaryPattern: sidecar.findPrimaryPattern,
              findSidecarReplace: sidecar.findSidecarReplace,
              required: sidecar.required,
            });
          }

          // Rename and move file
          await renameAndMoveFile({
            dryRun: task.dryRun,
            srcLibraryId,
            srcLibraryName,
            srcFilePath: `/${stripExtraSlashes(srcFile.parent_dir as string)}/${
              srcFile.name
            }`,
            dstLibraryId,
            dstLibraryName,
            dstFilePath: `${dstDirectory}/${dstFileName}`,
          });

          // Rename and move sidecar files
          for (const sidecar of task.sidecars) {
            const sidecarFilenameOld = getSidecarFilename({
              name: srcFile.name as string,
              find: sidecar.findPrimaryPattern,
              replace: sidecar.findSidecarReplace,
            });
            const sidecarFilenameNew = getSidecarFilename({
              name: dstFileName as string,
              find: sidecar.namePrimaryPattern,
              replace: sidecar.nameSidecarReplace,
            });
            const sidecarFileExists = await fileExists({
              libraryId: srcLibraryId,
              path: `/${stripExtraSlashes(
                srcFile.parent_dir as string
              )}/${sidecarFilenameOld}`,
            });
            if (sidecarFileExists) {
              await renameAndMoveFile({
                dryRun: task.dryRun,
                srcLibraryId,
                srcLibraryName,
                srcFilePath: `/${stripExtraSlashes(
                  srcFile.parent_dir as string
                )}/${sidecarFilenameOld}`,
                dstLibraryId,
                dstLibraryName,
                dstFilePath: `${dstDirectory}/${sidecarFilenameNew}`,
              });
            }
          }
        } catch (err) {
          logger.error(err.toString());
        }
      }

      // Hash a list of all current files for comparison at start of next loop
      libraryHashes = {
        ...libraryHashes,
        [listHashKey]: getListHash(srcFiles),
      };

      // Log completion of task
      logger.verbose(`Task "${task.name}" completed`);
    }
  } catch (err) {
    if (err.response) {
      const { status, statusText } = err.response;
      const { method, path } = err.response.request;
      logger.error(
        `${method} to ${path} failed with ${status} (${statusText})`
      );
    } else {
      logger.error(err.toString());
    }
  }
}
