FROM node:14.17.0-alpine AS dev
RUN apk --no-cache add git perl

FROM node:14.17.0-alpine AS build
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn install
COPY . .
RUN yarn run build

FROM node:14.17.0-alpine AS prod
RUN apk --no-cache add perl
ENV NODE_ENV=production
WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn install --production
COPY --from=build /usr/src/app/dist/ ./dist
CMD [ "node", "dist/index.js" ]
